<!DOCTYPE html>
<html>
<head>
    <!-- Page title -->
    <title>Meu Perfil | Residence Online</title>
    <?php $this->load->view("morador/inc/head_basico"); ?>
    <!-- CALENDÁRIO -->
    <link rel="stylesheet" href="/vendor/fullcalendar/dist/fullcalendar.print.css" media='print'/>
    <link rel="stylesheet" href="/vendor/fullcalendar/dist/fullcalendar.min.css" />
    <link rel="stylesheet" href="/vendor/fooTable/css/footable.core.min.css" />
    <!-- CROPPER -->
    <link rel="stylesheet" href="/assets/css/imgpicker.css">

    <!-- X-EDITABLE -->
    <link rel="stylesheet" href="/vendor/xeditable/bootstrap3-editable/css/bootstrap-editable.css" />
    <style type="text/css">
        .editable-padding{
            padding: 5px 0px;
            display: block;
        }
        .container-image {
            position: relative;
            width: 80%;
        }

        .image {
          opacity: 1;
          display: block;
          width: 100%;
          height: auto;
          transition: .5s ease;
          backface-visibility: hidden;
      }

      .middle {
          transition: .5s ease;
          opacity: 0;
          position: absolute;
          top: 50%;
          left: 50%;
          width: 100%;
          height: 100%;
          transform: translate(-50%, -50%);
          -ms-transform: translate(-50%, -50%)
      }

      .container-image:hover .image {
          opacity: 0.3;
      }

      .container-image:hover .middle {
          opacity: 1;
      }

      .text {
          background-color: transparent;
          color: #333;
          border:none;
          width: 100%;
          height: 100%;
          box-shadow: none;
          font-size: 16px;
          padding: 16px 32px;
      }
  </style>


</head>
<body>
    <?php $this->load->view("morador/inc/menu_lateral") ?>

    <!-- Main Wrapper -->
    <div id="wrapper">
        <div class="normalheader">
            <div class="hpanel">
                <div class="panel-body">
                    <a data-toggle="modal" data-target="#alterar_senha" class="btn btn-info btn-lg btn-outline pull-right hidden-xs"><i class="fa fa-user-o"></i> Alterar Login/Senha</a>
                    <h2 class="font-light m-b-xs">
                        <?= $usuario->nome ?>
                    </h2>
                    <a data-toggle="modal" data-target="#alterar_senha" class="btn btn-info btn-lg btn-outline btn-block visible-xs"><i class="fa fa-user-o"></i> Alterar Login/Senha</a>
                    
                    <h4 class="text-success text-center"><?= $this->session->flashdata("mensagem_perfil") ?></h4>
                </div>
            </div>
        </div>
        <div class="content animate-panel">
            <div class="row">
                <div class="col-lg-12">
                    <div class="hpanel">
                        <div class="panel-heading hbuilt">
                            Meus Dados
                        </div>
                        <div class="panel-body">
                            <h4 class="text-info text-center" style="margin-bottom: 25px;"><strong>Dica! </strong>Clique sobre a informação que deseja alterar.</h4>
                            <form action="/" method="post">
                                <div class="col-md-3 col-md-offset-0 col-sm-6 col-sm-offset-3 col-xs-12">
                                    <div class="container-image">
                                      <img src="<?= ($usuario->foto ? '/imagens/moradores/'.$usuario->foto.'?'.uniqid() : '//gravatar.com/avatar/0?d=mm&s=150') ?>" alt="Avatar" class="image" style="width:100%" id="avatar2" >
                                      <div class="middle">
                                        <button class="text" type="button" data-ip-modal="#avatarModal">
                                            <i class="pe-7s-camera fa-2x"></i><br>
                                            Alterar Imagem</button>
                                            <input type="hidden" name="foto" id="url_imagem" value="<?= $usuario->foto ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix visible-sm visible-xs" style="margin-bottom: 20px;"></div>
                                <div class="col-md-3 form-group animated-panel zoomIn" style="animation-delay: 0.4s;">
                                    <label>Nome:</label>
                                    <div class="clearfix"></div>
                                    <a href="#" id="nome" data-type="text" data-pk="<?= $usuario->id ?>" data-name="nome" data-title="Nome" class="editable editable-click editable-padding"><?= $usuario->nome ?></a>
                                </div>
                                <div class="col-md-3 form-group animated-panel zoomIn">
                                    <label>CPF:</label>
                                    <div class="clearfix"></div>
                                    <a href="#" id="cpf" data-type="text" data-pk="<?= $usuario->id ?>" data-name="cpf" data-title="CPF" class="editable-padding"><?= $usuario->cpf ?></a>
                                </div>
                                <div class="col-md-3 form-group animated-panel zoomIn">
                                    <label>RG:</label>
                                    <div class="clearfix"></div>
                                    <a href="#" id="rg" data-type="text" data-pk="<?= $usuario->id ?>" data-name="rg" data-title="RG" class="editable editable-click editable-padding"><?= $usuario->rg ?></a>
                                </div>
                                <!-- <div class="col-md-3 form-group animated-panel zoomIn">
                                <label>Naturalidade:</label>
                                <div class="clearfix"></div>
                                <a href="#" id="naturalidade" data-type="text" data-pk="<?= $usuario->id ?>" data-name="naturalidade" data-title="Naturalidade" class="editable editable-click editable-padding"><?= $usuario->naturalidade ?></a>
                            </div> -->
                            <div class="col-md-3 form-group animated-panel zoomIn">
                                <label>Sexo:</label>
                                <div class="clearfix"></div>
                                <a href="#" id="sexo" data-type="select" data-pk="<?= $usuario->id ?>" data-title="Sexo" class="editable-padding"></a>
                            </div>
                            <div class="col-md-3 form-group animated-panel zoomIn">
                                <label>Data de nascimento:</label>
                                <div class="clearfix"></div>
                                <a href="#" id="data_nascimento" data-type="text" data-pk="<?= $usuario->id ?>" data-name="data_nascimento" data-title="Data de nascimento" class="editable-padding"><?= $usuario->data_nascimento ?></a>
                            </div>
                            <div class="col-md-3 form-group animated-panel zoomIn">
                                <label>Email:</label>
                                <div class="clearfix"></div>
                                <a href="#" id="email" data-type="text" data-pk="<?= $usuario->id ?>" data-name="email" data-title="Email" class="editable editable-click editable-padding"><?= $usuario->email ?></a>
                            </div>
                            <div class="col-md-3 form-group animated-panel zoomIn">
                                <label>Fone:</label>
                                <div class="clearfix"></div>
                                <a href="#" id="fone" data-type="text" data-pk="<?= $usuario->id ?>" data-name="fone" data-title="Fone" class="editable-padding"><?= $usuario->fone ?></a>
                            </div>
                            <div class="col-md-3 form-group animated-panel zoomIn">
                                <label>Cel1:</label>
                                <div class="clearfix"></div>
                                <a href="#" id="cel1" data-type="text" data-pk="<?= $usuario->id ?>" data-name="cel1" data-title="Cel 1" class="editable-padding"><?= $usuario->cel1 ?></a>
                            </div>
                            <div class="col-md-3 form-group animated-panel zoomIn">
                                <label>Cel2:</label>
                                <div class="clearfix"></div>
                                <a href="#" id="cel2" data-type="text" data-pk="<?= $usuario->id ?>" data-name="cel2" data-title="Cel 2" class="editable-padding"><?= $usuario->cel2 ?></a>
                            </div>
                            <!-- <div class="col-md-4 form-group animated-panel zoomIn">
                                <label>Endereço:</label>
                                <div class="clearfix"></div>
                                <a href="#" id="endereco" data-type="text" data-pk="<?= $usuario->id ?>" data-name="endereco" data-title="Endereço" class="editable editable-click editable-padding"><?= $usuario->endereco ?></a>
                            </div> -->
                            <!-- <div class="col-md-4 form-group animated-panel zoomIn">
                                <label>Complemento:</label>
                                <div class="clearfix"></div>
                                <a href="#" id="complemento" data-type="text" data-pk="<?= $usuario->id ?>" data-name="complemento" data-title="Complemento" class="editable editable-click editable-padding"><?= $usuario->complemento ?></a>
                            </div> -->
                            <!-- <div class="col-md-4 form-group animated-panel zoomIn">
                                <label>Bairro:</label>
                                <a href="#" id="bairro" data-type="text" data-pk="<?= $usuario->id ?>" data-name="bairro" data-title="Bairro" class="editable editable-click editable-padding"><?= $usuario->bairro ?></a>
                            </div> -->
                            <!-- <div class="col-md-3 form-group animated-panel zoomIn">
                                <label>Cidade:</label>
                                <div class="clearfix"></div>
                                <a href="#" id="cidade" data-type="text" data-pk="<?= $usuario->id ?>" data-name="cidade" data-title="Cidade" class="editable editable-click editable-padding"><?= $usuario->cidade ?></a>
                            </div> -->
                            <!-- <div class="col-md-1 form-group animated-panel zoomIn">
                                <label>UF:</label>
                                <div class="clearfix"></div>
                                <a href="#" id="uf" data-type="text" data-pk="<?= $usuario->id ?>" data-name="uf" data-title="UF" class="editable-padding"><?= $usuario->uf ?></a>
                            </div> -->
                            <!-- <div class="col-md-4 form-group animated-panel zoomIn">
                                <label>CEP:</label>
                                <div class="clearfix"></div>
                                <a href="#" id="cep" data-type="text" data-pk="<?= $usuario->id ?>" data-name="cep" data-title="CEP" class="editable-padding"><?= $usuario->cep ?></a>
                            </div> -->
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="hpanel">
                    <div class="panel-heading hbuilt">
                        Minha Residência
                    </div>
                    <div class="panel-body">
                        <div class="col-lg-2">
                            <i class="fa fa-building-o fa-5x"></i>
                        </div>
                        <div class="col-lg-10">
                            <h4>Localização: <?= $usuario->nome_localizacao ?></h4>
                            <h4>Residência: <?= $usuario->nome_residencia ?></h4>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="hpanel">
                    <div class="panel-heading hbuilt">
                        Meus Boxes / Garagens
                    </div>
                    <div class="panel-body">
                        <div class="col-lg-2">
                            <i class="fa fa-flag fa-5x"></i>
                        </div>
                        <div class="col-lg-10">
                            <?php if ($boxes == null): ?>
                                <h4 class="text-info">Nenhum box vinculado à sua residência.</h4>
                            <?php endif ?>
                            <?php foreach ($boxes as $box): ?>
                                <div class="col-lg-3 col-md-3">
                                    <h4>Box / Garagem: <strong><?= $box->nome ?></strong></h4>
                                </div>
                            <?php endforeach ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php $this->load->view("morador/inc/footer"); ?>
</div>
<!-- Avatar Modal -->
<div class="ip-modal" id="avatarModal">
    <div class="ip-modal-dialog">
        <div class="ip-modal-content">
            <div class="ip-modal-header">
                <a class="ip-close" title="Close">&times;</a>
                <h4 class="ip-modal-title">Enviar imagem</h4>
            </div>
            <div class="ip-modal-body">
                <div class="btn btn-primary ip-upload">Enviar <input type="file" name="file" class="ip-file"></div>
                <button type="button" class="btn btn-primary ip-webcam">Webcam</button>
                <button type="button" class="btn btn-info ip-edit">Editar</button>
                <button type="button" class="btn btn-danger ip-delete">Excluir</button>

                <div class="alert ip-alert"></div>
                <div class="ip-info">Para editar a imagem, clique e arraste o mouse criando uma região. Solte o mouse e clique em "Salvar"</div>
                <div class="ip-preview"></div>
                <div class="ip-rotate">
                    <button type="button" class="btn btn-default ip-rotate-ccw" title="Rotate counter-clockwise"><i class="icon-ccw"></i></button>
                    <button type="button" class="btn btn-default ip-rotate-cw" title="Rotate clockwise"><i class="icon-cw"></i></button>
                </div>
                <div class="ip-progress">
                    <div class="text">Enviando</div>
                    <div class="progress progress-striped active"><div class="progress-bar"></div></div>
                </div>
            </div>
            <div class="ip-modal-footer">
                <div class="ip-actions">
                    <button type="button" class="btn btn-success ip-save">Salvar</button>
                    <button type="button" class="btn btn-primary ip-capture">Capturar</button>
                    <button type="button" class="btn btn-default ip-cancel">Cancelar</button>
                </div>
                <button type="button" class="btn btn-default ip-close">Fechar</button>
            </div>
        </div>
    </div>
</div>
<!-- end Modal -->

<!-- Modal alterar login/senha -->
<div class="modal fade" id="alterar_senha" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="color-line"></div>
            <div class="modal-header text-center">
                <h4 class="modal-title">Alterar login/senha</h4>
                <h5 class="text-danger" id="modal_alterar_senha_erro"></h5>
            </div>
            <div class="modal-body">
                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href=".login">Login</a></li>
                    <li class=""><a data-toggle="tab" href="#senha">Senha</a></li>
                </ul>
                <div class="tab-content">
                    <div id="" class="tab-pane active login">
                        <div class="panel-body">
                            <h5><?= $this->session->flashdata("mensagem_troca_login") ?></h5>
                            <div class="form-group">
                                <label>Novo login:</label>
                                <input required type="text" id="novo_login" class="form-control" placeholder="Login" />
                            </div>
                            <div class="pull-right">
                                <button class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                <button type="button" class="btn btn-primary" id="salvar_login">Salvar novo login</button>
                            </div>
                        </div>
                    </div>
                    <div id="senha" class="tab-pane">
                        <div class="panel-body">
                            <h5><?= $this->session->flashdata("mensagem_troca_senha") ?></h5>
                            <div class="form-group">
                                <label>Senha antiga:</label>
                                <input required type="password" id="senha_antiga" class="form-control" placeholder="********" />
                            </div>
                            <div class="form-group">
                                <label>Nova senha:</label>
                                <input required type="password" id="nova_senha" class="form-control" placeholder="********" />
                            </div>
                            <div class="form-group">
                                <label>Confirmação da nova senha:</label>
                                <input required type="password" id="confirmacao_de_senha" class="form-control" placeholder="********" />
                            </div>
                            <div class="pull-right">
                                <button class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                <button type="button" class="btn btn-primary" id="salvar_senha">Salvar nova senha</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal alterar login/senha -->

<?php $this->load->view("morador/inc/scripts_gerais") ?>
<!-- JavaScript CROPP -->
<script src="/assets/js/jquery.Jcrop.min.js"></script>
<script src="/assets/js/jquery.imgpicker.js"></script>
<script src="/scripts/jquery.mask.min.js"></script>
<script src="/vendor/xeditable/bootstrap3-editable/js/bootstrap-editable.min.js"></script>
<script>

    var subTipos;
    $(function() {
        // Avatar setup
        $('#avatarModal').imgPicker({
            url: '/server/upload_avatar.php',
            aspectRatio: 1,
            data: {
                uniqid: '<?= ($usuario->foto != null && $usuario->foto != "" ? $usuario->foto : uniqid()) ?>',
                id: <?= $usuario->id ?>
            },
            deleteComplete: function() {
                $('#avatar2').attr('src', '//gravatar.com/avatar/0?d=mm&s=150');
                $.post("/perfil/atualizar_foto", {
                    foto: "",
                    id: <?= $usuario->id ?>
                }, function(result){

                });
                this.modal('hide');
            },
            uploadSuccess: function(image) {
                // Calculate the default selection for the cropper
                var select = (image.width > image.height) ?
                [(image.width-image.height)/2, 0, image.height, image.height] :
                [0, (image.height-image.width)/2, image.width, image.width];      
                this.options.setSelect = select;
            },
            cropSuccess: function(image) {
                $('#avatar2').attr('src', '//gravatar.com/avatar/0?d=mm&s=150');
                $('#avatar2').attr('src', "/imagens/moradores/"+image.name);
                $.post("/perfil/atualizar_foto", {
                    foto: image.name,
                    id: <?= $usuario->id ?>
                }, function(result){
                    location.reload();
                });
                this.modal('hide');
            }
        });


        $("#cpf").mask("000.000.000-00");

        var maskBehavior = function (val) {
           return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
       },
       options = {onKeyPress: function(val, e, field, options) {
           field.mask(maskBehavior.apply({}, arguments), options);
       }
   };

   $('.fone').mask(maskBehavior, options);
        // $("#fone1").mask("(00)00000-000?0");
        // $("#cel1").mask("(00)00000-000?0");
        // $("#cel2").mask("(00)00000-000?0");
        $("#data_nascimento").mask("00/00/0000");
        $("#cep").mask("00000-000");

        $("#usuario_tipo").change(function(){
            atualizar_subTipos();
        });

        $.fn.editable.defaults.mode = 'popup';
        $.fn.editable.defaults.url = '/perfil/editable';

        $('.editable').editable({
            emptytext: 'Não informado',
            ajaxOptions: {
                dataType: 'json'
            },
            success: function(response, newValue) {
                if(!response) {
                    return "Erro desconhecido";
                }

                if(response.success === false) {
                    return response.msg;
                }
            }
        });

        $('#cpf').editable({
            tpl:'   <input type="text" id ="editable_cpf" class="form-control input-sm" style="padding-right: 24px;">',
            ajaxOptions: {
                dataType: 'json'
            },
            emptytext: 'Não inf.',
            success: function(response, newValue) {
                if(!response) {
                    return "Erro desconhecido";
                }

                if(response.success == false) {
                    return response.msg;
                }
            }
        }).on('shown',function(){
            $("input#editable_cpf").mask("999.999.999-99");
        });

        $('#data_nascimento').editable({
            tpl:'   <input type="text" id ="editable_data_nascimento" class="form-control input-sm" style="padding-right: 24px;">',
            ajaxOptions: {
                dataType: 'json'
            },
            emptytext: 'Não inf.',
            success: function(response, newValue) {
                if(!response) {
                    return "Erro desconhecido";
                }

                if(response.success == false) {
                    return response.msg;
                }
            }
        }).on('shown',function(){
            $("input#editable_data_nascimento").mask("99/99/9999");
        });

        $('#fone, #cel1, #cel2').editable({
            tpl:'   <input type="text" id ="editable_fone" class="form-control input-sm" style="padding-right: 24px;">',
            ajaxOptions: {
                dataType: 'json'
            },
            emptytext: 'Não inf.',
            success: function(response, newValue) {
                if(!response) {
                    return "Erro desconhecido";
                }

                if(response.success == false) {
                    return response.msg;
                }
            }
        }).on('shown',function(){
            var maskBehavior = function (val) {
               return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
           },
           options = {onKeyPress: function(val, e, field, options) {
               field.mask(maskBehavior.apply({}, arguments), options);
           }
       };
       $('#editable_fone').mask(maskBehavior, options);
   });

        $('#cep').editable({
            tpl:'   <input type="text" id ="cep" class="form-control input-sm" style="padding-right: 24px;">',
            ajaxOptions: {
                dataType: 'json'
            },
            emptytext: 'Não inf.',
            success: function(response, newValue) {
                if(!response) {
                    return "Erro desconhecido";
                }

                if(response.success == false) {
                    return response.msg;
                }
            }
        }).on('shown',function(){
            $("input#cep").mask("99999-999");
        });

        $('#uf').editable({
            emptytext: '??',
            ajaxOptions: {
                dataType: 'json'
            },
            success: function(response, newValue) {
                if(!response) {
                    return "Erro desconhecido";
                }

                if(response.success === false) {
                    return response.msg;
                }
            }
        });

        $('#usuario_tipo').editable({
            value: "<?= $usuario->usuario_tipo ?>",
            source: [
            {value: 'USUARIO', text: 'Morador'},
            {value: 'PRESTADOR', text: 'Prestador'},
            {value: 'COLABORADOR', text: 'Colaborador'},
            {value: 'VISITANTE', text: 'Visitante'}
            ],
            ajaxOptions: {
                dataType: 'json'
            },
            emptytext: 'Não informado',
            success: function(response, newValue) {
                if(!response) {
                    return "Erro desconhecido";
                }

                if(response.success === false) {
                    return response.msg;
                }

                $('#tipo').editable("destroy");
                atualizar_subTipos(newValue);
            }
        });

        iniciar_subTipos();

        function atualizar_subTipos(newValue){
            $.post("/perfil/editable_atualizar_subTipos", {
                tipo: newValue
            }, function(result){
                subTipos = result;
                $('#tipo').editable({
                    source:
                    JSON.parse(subTipos)
                    ,
                    ajaxOptions: {
                        dataType: 'json'
                    },
                    emptytext: 'Não informado',
                    success: function(response, newValue) {
                        if(!response) {
                            return "Erro desconhecido";
                        }

                        if(response.success === false) {
                            return response.msg;
                        }
                    }
                });
                $("#tipo").editable("show");
            });
        }

        $('#localizacao').editable({
            value: "<?= $usuario->localizacao ?>",
            source: [
            <?php
            $i = 0;
            foreach ($localizacoes as $localizacao): ?>
            {value: "<?= $localizacao->id ?>", text: "<?= $localizacao->nome ?>"}<?= ($i < sizeof($localizacoes)-1 ? "," : "") ?>
            <?php
            $i++;
            endforeach ?>
            ],
            ajaxOptions: {
                dataType: 'json'
            },
            emptytext: 'Não informado',
            success: function(response, newValue) {
                if(!response) {
                    return "Erro desconhecido";
                }

                if(response.success === false) {
                    return response.msg;
                }

                $('#residencia').editable("destroy");
                atualizar_residencias(newValue);
            }
        });
        iniciar_residencias();

        function atualizar_residencias(newValue){
            $.post("/perfil/editable_atualizar_residencias", {
                localizacao: newValue
            }, function(result){
                subTipos = result;
                $('#residencia').editable({
                    source:
                    JSON.parse(subTipos)
                    ,
                    ajaxOptions: {
                        dataType: 'json'
                    },
                    emptytext: 'Não informado',
                    success: function(response, newValue) {
                        if(!response) {
                            return "Erro desconhecido";
                        }

                        if(response.success === false) {
                            return response.msg;
                        }
                    }
                });
                $("#residencia").editable("show");
            });
        }

        $('#sexo').editable({
            value: "<?= $usuario->sexo ?>",
            source: [
            {value: 'M', text: 'Masculino'},
            {value: 'F', text: 'Feminino'},
            ],
            ajaxOptions: {
                dataType: 'json'
            },
            emptytext: 'Não informado',
            success: function(response, newValue) {
                if(!response) {
                    return "Erro desconhecido";
                }

                if(response.success === false) {
                    return response.msg;
                }
            }
        });

        $('#situacao').editable({
            value: "<?= $usuario->situacao ?>",
            source: [
            {value: '1', text: 'Liberado'},
            {value: '0', text: 'Bloqueado'},
            ],
            ajaxOptions: {
                dataType: 'json'
            },
            emptytext: 'Não informado',
            success: function(response, newValue) {
                if(!response) {
                    return "Erro desconhecido";
                }

                if(response.success === false) {
                    return response.msg;
                }
            }
        });

        $("#salvar_login").click(function(){
            item = $(this);
            anterior = item.html();
            item.html("<i class='fa fa-cog fa-spin'></i>");
            $.post("/perfil/ajax_alterar_login", {
                login: $("#novo_login").val(),
            }, function(result){
                item.html(anterior);
                if(result == ""){
                    $('#alterar_senha').modal('hide');
                    $("#modal_alterar_senha_erro").html("");
                    location.reload();
                }else
                $("#modal_alterar_senha_erro").html(result);
            });
        });

        $("#salvar_senha").click(function(){
            item = $(this);
            anterior = item.html();
            item.html("<i class='fa fa-cog fa-spin'></i>");
            $.post("/perfil/ajax_alterar_senha", {
                senha_antiga: $("#senha_antiga").val(),
                nova_senha: $("#nova_senha").val(),
                confirmacao_de_senha: $("#confirmacao_de_senha").val(),
            }, function(result){
                item.html(anterior);
                if(result == ""){
                    $('#alterar_senha').modal('hide');
                    $("#modal_alterar_senha_erro").html("");
                    location.reload();
                }else
                $("#modal_alterar_senha_erro").html(result);
            });
        });
    });

function iniciar_subTipos(){
    $('#tipo').editable("destroy");
    $.post("/perfil/editable_atualizar_subTipos", {
        tipo: "<?= $usuario->usuario_tipo ?>",
    }, function(result){
        subTipos = result;
        $('#tipo').editable({
            value: "<?= $usuario->tipo ?>",
            source:
            JSON.parse(subTipos)
            ,
            ajaxOptions: {
                dataType: 'json'
            },
            emptytext: 'Não informado',
            validate: function(value) {
                if($.trim(value) == '') return 'Selecione um sub-tipo.';
            },
            success: function(response, newValue) {
                if(!response) {
                    return "Erro desconhecido";
                }

                if(response.success === false) {
                    return response.msg;
                }
            }
        });
    });
}

function iniciar_localizacoes(){
    $('#localizacao').editable("destroy");
    $.post("/perfil/editable_atualizar_localizacoes", {}, function(result){
        localizacoes = result;
        console.log(result);
        $('#localizacao').editable({
            value: "<?= $usuario->localizacao ?>",
            source:
            JSON.parse(localizacoes)
            ,
            ajaxOptions: {
                dataType: 'json'
            },
            emptytext: 'Não informado',
            validate: function(value) {
                if($.trim(value) == '') return 'Selecione uma localização.';
            },
            success: function(response, newValue) {
                if(!response) {
                    return "Erro desconhecido";
                }

                if(response.success === false) {
                    return response.msg;
                }

                $('#residencia').editable("destroy");
                atualizar_residencias(newValue);
            }
        });
    });
}

function iniciar_residencias(){
    $('#residencia').editable("destroy");
    $.post("/perfil/editable_atualizar_residencias", {
        localizacao: $("#localizacao").editable("getValue").localizacao,
    }, function(result){
        localizacoes = result;
        $('#residencia').editable({
            value: "<?= $usuario->residencia ?>",
            source:
            JSON.parse(localizacoes)
            ,
            ajaxOptions: {
                dataType: 'json'
            },
            emptytext: 'Não informado',
            validate: function(value) {
                if($.trim(value) == '') return 'Selecione uma residência.';
            },
            success: function(response, newValue) {
                if(!response) {
                    return "Erro desconhecido";
                }

                if(response.success === false) {
                    return response.msg;
                }
            }
        });
    });
}
</script>
</body>
</html>