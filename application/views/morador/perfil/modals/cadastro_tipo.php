<a class="btn btn-info btn-outline input-group-addon" data-toggle="modal" data-target="#cadastro_tipo"><i class="fa fa-plus"></i></a>
<div class="modal fade" id="cadastro_tipo" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="color-line"></div>
            <div class="modal-header text-center">
                <h4 class="modal-title">Cadastrar sub-tipo de usuário</h4>
                <h5 class="text-danger" id="modal_cadastro_tipo_erro"></h5>
            </div>
            <div class="modal-body">
            	<div class="form-group">
            		<label>Tipo:</label>
            		<select required class="form-control" id="tipo_primario">
        			    <option value="">Selecione...</option>
        			    <option value="USUARIO">Morador</option>
        			    <option value="PRESTADOR">Prestador</option>
        			    <option value="COLABORADOR">Colaborador</option>
        			    <option value="VISITANTE">Visitante</option>
            		</select>
            	</div>

            	<div class="form-group">
            		<label>Novo sub-tipo:</label>
            		<input required class="form-control" type="text" id="nome_subtipo" />
            	</div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-primary" id="cadastrar_tipo">Salvar</button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
	$(function(){
		$("#cadastrar_tipo").click(function(){
            item = $(this);
            anterior = item.html();
            item.html("<i class='fa fa-cog fa-spin'></i>");
            $.post("/moradores/ajax_cadastro_tipo", {
                tipo: $("#tipo_primario").val(),
                nome: $("#nome_subtipo").val()
            }, function(result){
                item.html(anterior);
				if(result == ""){
					$("#tipo_primario").val("");
					$("#nome_subtipo").val("");
					$('#cadastro_tipo').modal('hide');
					$("#modal_cadastro_tipo_erro").html("");
                    iniciar_subTipos();
				}else
		        	$("#modal_cadastro_tipo_erro").html(result);
		    });
		});
	});
</script>