<!DOCTYPE html>
<html>
<head>
    <!-- Page title -->
    <title>Residence Online</title>
    <?php $this->load->view("morador/inc/head_basico"); ?>
</head>
<body>
<?php $this->load->view("morador/inc/menu_lateral") ?>

<!-- Main Wrapper -->
<div id="wrapper">
    
    <div class="row content animated-panel">
    </div>
    <div class="container_modal"></div>
    <?php $this->load->view("morador/inc/footer"); ?>
</div>
<?php $this->load->view("morador/inc/scripts_gerais") ?>
</body>
</html>