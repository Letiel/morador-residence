<div class="modal fade" id="reservar" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="color-line"></div>
            <div class="modal-header text-center">
                <h4 class="modal-title">Reservar <strong><?= $area->nome ?></strong></h4>
                <h5 class="text-danger" id="modal_reservar_erro"></h5>
            </div>
            <div class="modal-body">
                <div class="text-info text-center">
                    Horários disponíveis:
                    <ul>
                    <?php
                        $dias = array();
                        foreach ($horarios as $horario) {
                            switch($horario->dia_repetivel){
                                case 'SEGUNDA':
                                    $dia_semana = "Segunda";
                                    $dias['segunda'] = $dia_semana." - ".date("H:i", strtotime($horario->horario_inicio))." Hs. - ".date("H:i", strtotime($horario->horario_final))." Hs.";
                                    break;
                                case 'TERCA':
                                    $dia_semana = "Terça";
                                    $dias['terca'] = $dia_semana." - ".date("H:i", strtotime($horario->horario_inicio))." Hs. - ".date("H:i", strtotime($horario->horario_final))." Hs.";
                                    break;
                                case 'QUARTA':
                                    $dia_semana = "Quarta";
                                    $dias['quarta'] = $dia_semana." - ".date("H:i", strtotime($horario->horario_inicio))." Hs. - ".date("H:i", strtotime($horario->horario_final))." Hs.";
                                    break;
                                case 'QUINTA':
                                    $dia_semana = "Quinta";
                                    $dias['quinta'] = $dia_semana." - ".date("H:i", strtotime($horario->horario_inicio))." Hs. - ".date("H:i", strtotime($horario->horario_final))." Hs.";
                                    break;
                                case 'SEXTA':
                                    $dia_semana = "Sexta";
                                    $dias['sexta'] = $dia_semana." - ".date("H:i", strtotime($horario->horario_inicio))." Hs. - ".date("H:i", strtotime($horario->horario_final))." Hs.";
                                    break;
                                case 'SABADO':
                                    $dia_semana = "Sábado";
                                    $dias['sabado'] = $dia_semana." - ".date("H:i", strtotime($horario->horario_inicio))." Hs. - ".date("H:i", strtotime($horario->horario_final))." Hs.";
                                    break;
                                case 'DOMINGO':
                                    $dia_semana = "Domingo";
                                    $dias['domingo'] = $dia_semana." - ".date("H:i", strtotime($horario->horario_inicio))." Hs. - ".date("H:i", strtotime($horario->horario_final))." Hs.";
                                    break;
                                default:
                                    $dia_semana = date("d/m/Y", strtotime($horario->dia_repetivel));
                                    break;
                            }
                        }
                    ?>
                        <?php if (isset($dias['domingo'])): ?>
                            <li class="text-center text-info"><?= $dias['domingo'] ?></li>
                        <?php endif ?>
                        <?php if (isset($dias['segunda'])): ?>
                            <li class="text-center text-info"><?= $dias['segunda'] ?></li>
                        <?php endif ?>
                        <?php if (isset($dias['terca'])): ?>
                            <li class="text-center text-info"><?= $dias['terca'] ?></li>
                        <?php endif ?>
                        <?php if (isset($dias['quarta'])): ?>
                            <li class="text-center text-info"><?= $dias['quarta'] ?></li>
                        <?php endif ?>
                        <?php if (isset($dias['quinta'])): ?>
                            <li class="text-center text-info"><?= $dias['quinta'] ?></li>
                        <?php endif ?>
                        <?php if (isset($dias['sexta'])): ?>
                            <li class="text-center text-info"><?= $dias['sexta'] ?></li>
                        <?php endif ?>
                        <?php if (isset($dias['sabado'])): ?>
                            <li class="text-center text-info"><?= $dias['sabado'] ?></li>
                        <?php endif ?>
                    </ul>
                </div>
                <div class="form-group">
                    <label>Data de início:</label>
                    <input class="form-control" required type="text" id="horario_inicio">
                </div>
                <div class="form-group">
                    <label>Data de fim:</label>
                    <input class="form-control" required type="text" id="horario_final">
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-primary" id="reservar_area">Salvar</button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(function(){
        $("#reservar_area").click(function(){
            item = $(this);
            anterior = item.html();
            item.html("<i class='fa fa-cog fa-spin'></i>");
            $.post("/areas/ajax_reserva_area", {
                area: <?= $id_area ?>,
                horario_inicio: $("#horario_inicio").val(),
                horario_final: $("#horario_final").val(),
            }, function(result){
                item.html(anterior);
                if(result == ""){
                    $("#horario_inicio").val("");
                    $("#horario_final").val("");
                    $('#reservar').modal('hide');
                    $("#modal_reservar_erro").html("");
                    location.reload();
                }else
                    $("#modal_reservar_erro").html(result);
            });
        });

        $.datetimepicker.setLocale('pt-BR');
        $("#horario_inicio").datetimepicker({
            format:'d/m/Y H:i',
        });

        $("#horario_final").datetimepicker({
            format:'d/m/Y H:i',
        });

        $("#reservar").modal("show");
    });
</script>