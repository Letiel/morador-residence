<div class="modal fade" id="cadastro_convidado" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="color-line"></div>
            <div class="modal-header text-center">
                <h4 class="modal-title">Convidados</h4>
                <h5 class="text-danger" id="modal_cadastro_convidado_erro"></h5>
            </div>
            <div class="modal-body">
            <div id="retorno_cadastro_convidado"></div>
              <ul class="nav nav-tabs">
                  <?php if (strtotime($reserva->hora_fim) > strtotime(date("Y-m-d H:i:s"))): ?>
                      <li class="active"><a data-toggle="tab" href="#tab_adicionar">Adicionar</a></li>
                  <?php endif ?>
                  <li <?php if (strtotime($reserva->hora_fim) < strtotime(date("Y-m-d H:i:s"))): ?> class="active" <?php endif ?>><a data-toggle="tab" href="#tab_lista">Lista</a></li>
              </ul>
              <div class="tab-content">
                <?php if (strtotime($reserva->hora_fim) > strtotime(date("Y-m-d H:i:s"))): ?>
                  <div id="tab_adicionar" class="tab-pane active">
                      <div class="panel-body">
                          <div class="form-group">
                              <label>Nome completo:</label>
                              <input required type="text" id="nome_completo" class="form-control" placeholder="Nome completo" />
                          </div>
                          <div class="form-group">
                              <label>RG:</label>
                              <input required type="text" id="rg" class="form-control" placeholder="RG" />
                          </div>
                          <div class="pull-right">
                              <button type="button" class="btn btn-primary" id="salvar_convidado">Salvar novo convidado</button>
                          </div>
                      </div>
                  </div>
                <?php endif ?>
                  <div id="tab_lista" <?php if (strtotime($reserva->hora_fim) < strtotime(date("Y-m-d H:i:s"))): ?> class=" tab-pane active"
                  <?php else: ?>  class="tab-pane" <?php endif ?>>
                      <div class="panel-body">
                          <table class="table table-bordered">
                            <thead>
                              <tr>
                                <th>Nome</th>
                                <th>RG</th>
                                <th>Remover</th>
                              </tr>
                            </thead>
                            <tbody id="lista_convidados">
                              <?= $total_convidados == 0 ? "<tr><td colspan='3'><h2 class='text-info text-center'>Nenhum convidado.</h2></td></tr>" : "" ?>
                            <?php foreach ($convidados as $convidado): ?>
                              <tr>
                                <td><?= $convidado->nome ?></td>
                                <td><?= $convidado->rg ?></td>
                                <?php if (strtotime($reserva->hora_fim) > strtotime(date("Y-m-d H:i:s"))): ?>
                                  <td><button onclick="remover_convidado(<?= $convidado->id ?>)" class="btn btn-danger btn-outline" value="<?= $convidado->id ?>">Remover</button></td>
                                <?php endif ?>
                              </tr>
                            <?php endforeach ?>
                            </tbody>
                          </table>
                      </div>
                  </div>
              </div>

            </div>
            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">

  function atualizar_lista(){
    item = $("#lista_convidados");
    anterior = item.html();
    item.html("<i class='fa fa-cog fa-spin fa-5x'></i>");
    $.post("/areas/ajax_atualizar_convidados", {
        id: <?= $id_reserva ?>
    }, function(result){
        item.html(result);
    });
  }

  function remover_convidado(id){
    if(confirm("Tem certeza?")){
      $("#retorno_cadastro_convidado").html("");
      $.post("/areas/ajax_remover_convidado", {
          id: id
      }, function(result){
          if(result == ""){
            $("#retorno_cadastro_convidado").html("<h2 class='text-warning text-center'>Removido com sucesso!</h2>");
          }else
            $("#retorno_cadastro_convidado").html(result);
          atualizar_lista();
      });
    }
  }

	$(function(){
    $("#cadastro_convidado").modal("show");

		$("#salvar_convidado").click(function(){
      item = $(this);
      anterior = item.html();
      item.html("<i class='fa fa-cog fa-spin'></i>");
      $("#retorno_cadastro_convidado").html("");
      $.post("/areas/ajax_cadastro_convidado", {
          nome: $("#nome_completo").val(),
          rg: $("#rg").val(),
          reserva: <?= $id_reserva ?>
      }, function(result){
          item.html(anterior);
          if(result == ""){
            $("#nome_completo").val("");
            $("#rg").val("");
		        $("#retorno_cadastro_convidado").html("<h2 class='text-info text-center'>Cadastrado com sucesso!<br /><small>Verifique a lista.</small></h2>");
          }else
            $("#retorno_cadastro_convidado").html(result);
          atualizar_lista();
	    });
		});
	});
</script>