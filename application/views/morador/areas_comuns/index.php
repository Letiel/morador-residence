<!DOCTYPE html>
<html>
<head>
    <!-- Page title -->
    <title>Residence Online</title>
    <?php $this->load->view("morador/inc/head_basico"); ?>
    <!-- SELECT2 -->
    <link rel="stylesheet" href="/css/select2.min.css" />
    <link rel="stylesheet" href="/css/select2-bootstrap.css" />

    <link rel="stylesheet" type="text/css" href="/css/jquery.datetimepicker.min.css">

    <style type="text/css">
        ul{
            padding: 0;
        }
        li{
            list-style: none;
            padding: 0;
        }
    </style>
</head>
<body>
    <?php $this->load->view("morador/inc/menu_lateral") ?>

    <!-- Main Wrapper -->
    <div id="wrapper">

     <div class="normalheader ">
        <div class="hpanel">
            <div class="panel-body">
                <h2 class="font-light m-b-xs">Áreas Comuns</h2>
                <small><b>Dica:</b> Clique para expandir e reservar uma área!</small>
                <?= $this->session->flashdata("mensagem_reserva"); ?>
            </div>
        </div>
    </div>
    <div class="content">
        <div class="row">
         <div class="col-lg-12">
            <div class="hpanel panel-group" id="accordion" role="tablist" aria-multiselectable="true">

                <?php if ($areas == null): ?>
                    <div class="panel-body">
                        <h2 class="text-center text--primary">Nenhuma área</h2>
                    </div>
                <?php else: ?>
                    <?php foreach ($areas as$area): ?>
                        <div class="panel-body">
                            <a data-toggle="collapse" data-parent="#accordion" href="#<?= $area->id ?>" aria-expanded="false" class="collapsed">
                                <i class="fa fa-chevron-down pull-right text-muted"></i>
                                <?= $area->nome ?>
                            </a>
                            <div id="<?= $area->id ?>" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                                <hr>
                                <div class="col-md-12">
                                    <div class="row">
                                        <h2 class="m-b-xs text-indigo pull-left"><?= $area->nome ?></h2>
                                        <button class="btn btn-success reservar pull-right" value='<?= $area->id ?>'>Reservar</button>
                                    </div>
                                </div>
                                <p class="small"><?= nl2br($area->descricao) ?></p>
                                <p><b>Localização:</b> <?= ucfirst($area->nome_localizacao) ?></p>
                                <p><b>Responsável:</b> <?= $area->nome_responsavel ?></p>

                                <?php
                                $this->db->start_cache();
                                $this->db->where("horarios_areas.cnpj", $this->session->userdata("cnpj"));
                                $this->db->where("horarios_areas.area", $area->id);
                                $horarios = $this->db->get("horarios_areas");
                                $this->db->stop_cache();
                                $this->db->flush_cache();
                                $rows = $horarios->num_rows();
                                if($rows > 0){
                                    $horarios = $horarios->result();
                                    $dias = array();
                                    ?>
                                    <hr>
                                    <ul>
                                        <?php foreach ($horarios as $horario): ?>
                                            <?php
                                            switch($horario->dia_repetivel){
                                                case 'SEGUNDA':
                                                $dia_semana = "Segunda";
                                                $dias['segunda'] = $dia_semana." - ".date("H:i", strtotime($horario->horario_inicio))." Hs. - ".date("H:i", strtotime($horario->horario_final))." Hs.";
                                                break;
                                                case 'TERCA':
                                                $dia_semana = "Terça";
                                                $dias['terca'] = $dia_semana." - ".date("H:i", strtotime($horario->horario_inicio))." Hs. - ".date("H:i", strtotime($horario->horario_final))." Hs.";
                                                break;
                                                case 'QUARTA':
                                                $dia_semana = "Quarta";
                                                $dias['quarta'] = $dia_semana." - ".date("H:i", strtotime($horario->horario_inicio))." Hs. - ".date("H:i", strtotime($horario->horario_final))." Hs.";
                                                break;
                                                case 'QUINTA':
                                                $dia_semana = "Quinta";
                                                $dias['quinta'] = $dia_semana." - ".date("H:i", strtotime($horario->horario_inicio))." Hs. - ".date("H:i", strtotime($horario->horario_final))." Hs.";
                                                break;
                                                case 'SEXTA':
                                                $dia_semana = "Sexta";
                                                $dias['sexta'] = $dia_semana." - ".date("H:i", strtotime($horario->horario_inicio))." Hs. - ".date("H:i", strtotime($horario->horario_final))." Hs.";
                                                break;
                                                case 'SABADO':
                                                $dia_semana = "Sábado";
                                                $dias['sabado'] = $dia_semana." - ".date("H:i", strtotime($horario->horario_inicio))." Hs. - ".date("H:i", strtotime($horario->horario_final))." Hs.";
                                                break;
                                                case 'DOMINGO':
                                                $dia_semana = "Domingo";
                                                $dias['domingo'] = $dia_semana." - ".date("H:i", strtotime($horario->horario_inicio))." Hs. - ".date("H:i", strtotime($horario->horario_final))." Hs.";
                                                break;
                                                default:
                                                $dia_semana = date("d/m/Y", strtotime($horario->dia_repetivel));
                                                break;
                                            }
                                            ?>
                                        <?php endforeach ?>

                                        <h3 class="text-indigo"><i class="fa fa-clock-o"></i> Horários</h3>
                                        <?php if (isset($dias['domingo'])): ?>
                                            <li><?= $dias['domingo'] ?></li>
                                        <?php endif ?>
                                        <?php if (isset($dias['segunda'])): ?>
                                            <li><?= $dias['segunda'] ?></li>
                                        <?php endif ?>
                                        <?php if (isset($dias['terca'])): ?>
                                            <li><?= $dias['terca'] ?></li>
                                        <?php endif ?>
                                        <?php if (isset($dias['quarta'])): ?>
                                            <li><?= $dias['quarta'] ?></li>
                                        <?php endif ?>
                                        <?php if (isset($dias['quinta'])): ?>
                                            <li><?= $dias['quinta'] ?></li>
                                        <?php endif ?>
                                        <?php if (isset($dias['sexta'])): ?>
                                            <li><?= $dias['sexta'] ?></li>
                                        <?php endif ?>
                                        <?php if (isset($dias['sabado'])): ?>
                                            <li><?= $dias['sabado'] ?></li>
                                        <?php endif ?>
                                    </ul>
                                    <?php
                                }
                                ?>

                                <?php
                                $this->db->start_cache();
                                $this->db->where("reservas.usuario", $this->session->userdata("id"));
                                $this->db->where("reservas.area", $area->id);
                                $this->db->where("reservas.hora_inicio > NOW()");
                                $reservas = $this->db->get("reservas");
                                if($reservas->num_rows() > 0){
                                    $reservas = $reservas->result();
                                    ?>
                                    <hr>
                                    <h3 class='text-indigo'>Minhas Reservas</h3>
                                    <?php foreach ($reservas as $reserva): ?>
                                        <?php if ($reservas[0] != $reserva): ?>
                                            <hr>
                                        <?php endif ?>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <p style="margin: 8px 0;">
                                                    <?= date("d/m/Y H:i", strtotime($reserva->hora_inicio)) ?> - <?= date("d/m/Y H:i", strtotime($reserva->hora_fim)) ?> 
                                                </p>
                                                <p>
                                                    <?= ($reserva->autorizado ? "<span class='label label-success'>Confirmado!</span>" : "<span class='label label-info'>Não confirmado.</span>") ?>
                                                </p>
                                            </div>
                                            <div class="col-md-6 hidden-xs">
                                                <div class="btn-group pull-right" role="group" aria-label="...">
                                                 <button class="btn btn-info convidados" value="<?= $reserva->id ?>">Convidados</button>
                                                 <button class="btn btn-danger
                                                 cancelar" value="<?= $reserva->id ?>">Cancelar</button>
                                             </div>
                                         </div>
                                         <div class="col-md-6 visible-xs">
                                            <div class="btn-group" role="group" aria-label="...">
                                             <button class="btn btn-info convidados" value="<?= $reserva->id ?>">Convidados</button>
                                             <button class="btn btn-danger
                                             cancelar" value="<?= $reserva->id ?>">Cancelar</button>
                                         </div>
                                     </div>                                           
                                 </div>
                             <?php endforeach ?>
                             <?php
                         }
                         $this->db->stop_cache();
                         $this->db->flush_cache();
                         ?>

                     </div>
                 </div>
             <?php endforeach ?>
         <?php endif ?>
     </div>
 </div>
</div>
</div>





<div class="container_modal"></div>
<?php $this->load->view("morador/inc/footer"); ?>
</div>
<?php $this->load->view("morador/inc/scripts_gerais") ?>
<script src="/js/select2.full.min.js"></script>

<script type="text/javascript" src="/js/jquery.datetimepicker.full.min.js"></script>

<script type="text/javascript">
    $(function(){

        $(".reservar").click(function(){
            item = $(this);
            anterior = item.html();
            item.html("<i class='fa fa-cog fa-spin'></i>");
            $.post("/areas/reservar", {
                id: $(this).val()
            }, function(result){
                $(".container_modal").html(result);
                item.html(anterior);
            });
        });

        $(".convidados").click(function(){
            item = $(this);
            anterior = item.html();
            item.html("<i class='fa fa-cog fa-spin'></i>");
            $.post("/areas/convidados", {
                id: $(this).val()
            }, function(result){
                $(".container_modal").html(result);
                item.html(anterior);
            });
        });

        // $(".cancelar").click(function(){
        //     if(confirm("Tem certeza?")){
        //         item = $(this);
        //         anterior = item.html();
        //         item.html("<i class='fa fa-cog fa-spin'></i>");
        //         $.post("/areas/ajax_cancelar_reserva", {
        //             id: $(this).val()
        //         }, function(result){
        //             location.reload();
        //         });
        //     }
        // });

        $('.cancelar').click(function () {
            var value = $(this).val();
            var item = $(this);
            var anterior = item.html();
            swal({
                title: "Tem certeza?",
                text: "Você tem certeza que quer cancelar esta reserva?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Sim, quero cancelar!",
                cancelButtonText: "Cancelar",
            },
            function () {
            item.html("<i class='fa fa-cog fa-spin'></i>");
            swal("Cancelando!", "Estamos cancelando sua reserva", "warning");
             $.post("/areas/ajax_cancelar_reserva", {
                id: value
            }, function(result){
                swal("Cancelada!", "Reserva cancelada com sucesso", "success");
                item.html(anterior);
                location.reload();
            });
         });
        });
    });
</script>
</body>
</html>