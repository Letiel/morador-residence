<!DOCTYPE html>
<html>
<head>
    <!-- Page title -->
    <title>Residence Online</title>
    <?php $this->load->view("morador/inc/head_basico"); ?>
    <!-- SELECT2 -->
    <link rel="stylesheet" href="/css/select2.min.css" />
    <link rel="stylesheet" href="/css/select2-bootstrap.css" />

    <link rel="stylesheet" type="text/css" href="/css/jquery.datetimepicker.min.css">

    <style type="text/css">
        ul{
            padding: 0;
        }
        li{
            list-style: none;
            padding: 0;
        }
    </style>
</head>
<body>
    <?php $this->load->view("morador/inc/menu_lateral") ?>

    <!-- Main Wrapper -->
    <div id="wrapper">

       <div class="normalheader ">
        <div class="hpanel">
            <div class="panel-body">
               <a href="/reservas" class="btn btn-info btn-outline btn-lg pull-right hidden-xs"><i class="fa fa-calendar"></i> Reservas Futuras</a>
               <h2 class="font-light m-b-xs">Reservas Passadas</h2>
               <a href="/reservas" class="btn btn-info btn-outline btn-lg btn-block visible-xs"><i class="fa fa-calendar"></i> Reservas Futuras</a>
                <?= $this->session->flashdata("mensagem_reserva"); ?>
            </div>
        </div>
    </div>
    <div class="content">
        <div class="row">
           <div class="col-lg-12">
            <div class="hpanel panel-group" id="accordion" role="tablist" aria-multiselectable="true">

                <?php if ($reservas): ?>
                    <?php foreach ($reservas as $key => $reserva): ?>
                        <div class="panel-body">
                            <h4>Reservar para <b><?php echo $reserva->nome_area ?></b></h4>
                            <div class="pull-left">
                                <p style="margin: 8px 0;">
                                    <?= date("d/m/Y H:i", strtotime($reserva->hora_inicio)) ?> - <?= date("d/m/Y H:i", strtotime($reserva->hora_fim)) ?> 
                                </p>
                                <p>
                                    <?= ($reserva->autorizado ? "<span class='label label-success'>Confirmado!</span>" : "<span class='label label-info'>Não confirmado.</span>") ?>
                                </p>
                            </div>
                            <div class="btn-group pull-right hidden-xs" role="group" aria-label="...">
                                <button class="btn btn-info convidados" value="<?= $reserva->id ?>">Convidados</button>
                                <!-- <button class="btn btn-danger cancelar" value="<?= $reserva->id ?>">Cancelar</button> -->
                            </div>
                            <div class="btn-group visible-xs" role="group" aria-label="..." style="clear: both;">
                                <button class="btn btn-info convidados" value="<?= $reserva->id ?>">Convidados</button>
                                <!-- <button class="btn btn-danger cancelar" value="<?= $reserva->id ?>">Cancelar</button> -->
                            </div>
                        </div>
                    <?php endforeach ?>
                    <?php if ($paginacao): ?>
                        <div class="panel-body">
                            <center>
                                <?php echo $paginacao ?>
                            </center>
                        </div>
                    <?php endif ?>
                <?php else: ?>
                    <div class="panel-body">
                        <h2 class="text-center text--primary">Nenhuma reserva</h2>
                    </div>
                <?php endif ?>
            </div>
        </div>
    </div>
</div>





<div class="container_modal"></div>
<?php $this->load->view("morador/inc/footer"); ?>
</div>
<?php $this->load->view("morador/inc/scripts_gerais") ?>
<script src="/js/select2.full.min.js"></script>

<script type="text/javascript" src="/js/jquery.datetimepicker.full.min.js"></script>

<script type="text/javascript">
    $(function(){

        $(".reservar").click(function(){
            item = $(this);
            anterior = item.html();
            item.html("<i class='fa fa-cog fa-spin'></i>");
            $.post("/areas/reservar", {
                id: $(this).val()
            }, function(result){
                $(".container_modal").html(result);
                item.html(anterior);
            });
        });

        $(".convidados").click(function(){
            item = $(this);
            anterior = item.html();
            item.html("<i class='fa fa-cog fa-spin'></i>");
            $.post("/areas/convidados", {
                id: $(this).val()
            }, function(result){
                $(".container_modal").html(result);
                item.html(anterior);
            });
        });

        // $(".cancelar").click(function(){
        //     if(confirm("Tem certeza?")){
        //         item = $(this);
        //         anterior = item.html();
        //         item.html("<i class='fa fa-cog fa-spin'></i>");
        //         $.post("/areas/ajax_cancelar_reserva", {
        //             id: $(this).val()
        //         }, function(result){
        //             location.reload();
        //         });
        //     }
        // });

        $('.cancelar').click(function () {
            var value = $(this).val();
            var item = $(this);
            var anterior = item.html();
            swal({
                title: "Tem certeza?",
                text: "Você tem certeza que quer cancelar esta reserva?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Sim, quero cancelar!",
                cancelButtonText: "Cancelar",
            },
            function () {
                item.html("<i class='fa fa-cog fa-spin'></i>");
                swal("Cancelando!", "Estamos cancelando sua reserva", "warning");
                $.post("/areas/ajax_cancelar_reserva", {
                    id: value
                }, function(result){
                    swal("Cancelada!", "Reserva cancelada com sucesso", "success");
                    item.html(anterior);
                    location.reload();
                });
            });
        });
    });
</script>
</body>
</html>