<!DOCTYPE html>
<html>
<head>
    <!-- Page title -->
    <title>Veículos | Residence Online</title>
    <?php $this->load->view("morador/inc/head_basico"); ?>
    
    <style type="text/css">
        .btn-danger-clear{
            color: #e74c3c;
            background-color: transparent;
        }
    </style>
</head>
<body>
    <?php $this->load->view("morador/inc/menu_lateral") ?>

    <!-- Main Wrapper -->
    <div id="wrapper">


        <div class="normalheader ">
            <div class="hpanel">
                <div class="panel-body">
                    <a class="btn btn-info btn-lg btn-outline pull-right" href="/veiculos/cadastrar"><i class="fa fa-plus"></i> Adicionar veículo</a>
                    <h2 class="font-light m-b-xs">
                        Veículos
                    </h2>
                    <h2 class="text-info text-center"><?= $this->session->flashdata("mensagem_cadastro_veiculo"); ?></h2>
                </div>
            </div>
        </div>
        <div class="content">
            <div class="row">
                <?php if ($veiculos == null): ?>
                    <div class="col-md-12">
                        <div class="hpanel contact-panel">
                            <div class="panel-body">
                                <h2 class="text-info text-center">Nenhum veículo</h2>
                            </div>
                        </div>
                    </div>
                <?php else: ?>
                    <?php foreach ($veiculos as $veiculo): ?>
                        <div class="col-lg-3">
                            <div class="hpanel contact-panel">
                                <div class="panel-body">
                                    <?php if (strtotime($veiculo->data_alteracao) > strtotime($this->session->userdata('ultimo_login'))): ?>
                                        <span class="label label-success pull-right">Novo</span>
                                    <?php endif ?>
                                    <img alt="logo" class="img-circle m-b" src="<?= ($veiculo->foto != null) ? '/imagens/veiculos/'.$veiculo->foto : '/images/veiculo.png' ; ?>" alt="<?= $veiculo->placa ?>" title="<?= $veiculo->placa ?>">
                                    <h3><a href="/veiculos/ver/<?= $veiculo->id ?>"> <?= $veiculo->placa ?> </a></h3>
                                    <div class="text-muted font-bold m-b-xs">
                                        <?php $veiculo->cidade = ($veiculo->cidade != null) ? $veiculo->cidade : "" ; ?>  
                                        <?php $veiculo->uf = ($veiculo->uf != null) ? strtoupper($veiculo->uf) : "" ; ?>  
                                        <?= ucfirst($veiculo->cidade) ?>
                                        <?=($veiculo->cidade != null && $veiculo->uf != null) ? "," : " " ; ?>
                                        <?= $veiculo->uf ?>
                                        <?php if ($veiculo->cidade == null && $veiculo->uf == null): ?>
                                            <br>
                                        <?php endif ?>
                                    </div>
                                    <?php if ($veiculo->tipo_veiculo != null): ?>
                                        <p><b>Tipo:</b> <?= $veiculo->tipo_veiculo ?></p>
                                    <?php else: ?>
                                        <p><b>Tipo:</b> Não informado</p>
                                    <?php endif ?>
                                    <?php if ($veiculo->modelo != null): ?>
                                        <p><b>Modelo:</b> <?= $veiculo->modelo ?></p>
                                    <?php else: ?>
                                        <p><b>Modelo:</b> Não informado</p>
                                    <?php endif ?>
                                    <?php if ($veiculo->fabricante != null): ?>
                                        <p><b>Fabricante:</b> <?= $veiculo->fabricante ?></p>
                                    <?php else: ?>
                                        <p><b>Fabricante:</b> Não informado</p>
                                    <?php endif ?>
                                    <?php if ($veiculo->cor != null): ?>
                                        <p><b>Cor:</b> <?= $veiculo->cor ?></p>
                                    <?php else: ?>
                                        <p><b>Cor:</b> Não informado</p>
                                    <?php endif ?>
                                </div>
                                <div class="panel-footer contact-footer">
                                    <div class="row">
                                        <div class="col-md-6 border-right">
                                            <a href="/veiculos/ver/<?= $veiculo->id?>" class="btn btn-block btn-link"> Alterar</a>
                                        </div>
                                        <div class="col-md-6 border-right">
                                            <button class="btn btn-block btn-danger-clear deletar" value="<?= $veiculo->id ?>"> Excluir</button >
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach ?>
                    <?php endif ?>
                </div>
            </div>    


            <?php $this->load->view("morador/inc/footer"); ?>
        </div>



        <?php $this->load->view("morador/inc/scripts_gerais") ?>
        
        <script type="text/javascript">

            $('.deletar').click(function () {
                var value = $(this).val();
                var item = $(this);
                var anterior = item.html();
                swal({
                    title: "Tem certeza?",
                    text: "Você tem certeza que quer excluir este veículo?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Sim, quero excluir!",
                    cancelButtonText: "Cancelar",
                },
                function () {
                item.html("<i class='fa fa-cog fa-spin'></i>");
                 $.post( "/veiculos/deletar", {id: value}, function( data ) {
                    item.html(anterior);
                    location.reload();
              });
             });
            });
        </script>

    </body>
    </html>