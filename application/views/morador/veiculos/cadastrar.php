<!DOCTYPE html>
<html>
<head>
    <!-- Page title -->
    <title>Adicionar Veículo | Residence Online</title>
    <?php $this->load->view("morador/inc/head_basico"); ?>
    <!-- CALENDÁRIO -->
    <link rel="stylesheet" href="/vendor/fullcalendar/dist/fullcalendar.print.css" media='print'/>
    <link rel="stylesheet" href="/vendor/fullcalendar/dist/fullcalendar.min.css" />
    <link rel="stylesheet" href="/vendor/fooTable/css/footable.core.min.css" />
    <!-- CROPPER -->
    <link rel="stylesheet" href="/assets/css/imgpicker.css">
    <!-- SELECT2 -->
    <link rel="stylesheet" href="/css/select2.min.css" />
    <link rel="stylesheet" href="/css/select2-bootstrap.css" />
    <style type="text/css">
        .container-image {
            position: relative;
            width: 80%;
        }

        .image {
          opacity: 1;
          display: block;
          width: 100%;
          height: auto;
          transition: .5s ease;
          backface-visibility: hidden;
      }

      .middle {
          transition: .5s ease;
          opacity: 0;
          position: absolute;
          top: 50%;
          left: 50%;
          width: 100%;
          height: 100%;
          transform: translate(-50%, -50%);
          -ms-transform: translate(-50%, -50%)
      }

      .container-image:hover .image {
          opacity: 0.3;
      }

      .container-image:hover .middle {
          opacity: 1;
      }

      .text {
          background-color: transparent;
          color: #333;
          border:none;
          width: 100%;
          height: 100%;
          box-shadow: none;
          font-size: 16px;
          padding: 16px 32px;
      }
  </style>
</head>
<body>
    <?php $this->load->view("morador/inc/menu_lateral") ?>

    <!-- Main Wrapper -->
    <div id="wrapper">
        <div class="normalheader">
            <div class="hpanel">
                <div class="panel-body">

                    <h2 class="font-light m-b-xs">
                        Adicionar Veículo
                    </h2>

                </div>
            </div>
        </div>
        <div class="content animate-panel">
            <div class="row">
                <div class="col-lg-12">
                    <div class="hpanel">
                        <div class="panel-body">
                            <form action="/veiculos/cadastrar" method="post">
                             <div class="col-md-3 col-md-offset-0 col-sm-6 col-sm-offset-3 col-xs-12">
                                <div class="container-image">
                                    <img src="/images/veiculo.png" alt="Veículo" class="image" style="width:100%" id="avatar2" >
                                    <div class="middle">
                                        <button class="text" type="button" data-ip-modal="#avatarModal">
                                            <i class="pe-7s-camera fa-2x"></i><br>
                                            Selecionar Imagem</button>
                                            <input type="hidden" name="foto" id="url_imagem" value="<?= set_value('foto') ?>">
                                        </div>
                                    </div>                                    
                                </div>
                           <!--  <div class="pull-left col-md-3">
                                <div class="pull-left">
                                    <img src="/images/veiculo.png" id="avatar2" width="150"><br>
                                    <button type="button" class="btn btn-info btn-block" data-ip-modal="#avatarModal">Foto</button>
                                    <input type="hidden" name="foto" id="url_imagem" value="<?= set_value('foto') ?>">
                                </div>
                            </div> -->
                            <div class="clearfix visible-sm visible-xs"></div>
                            <div class="col-md-2 form-group animated-panel zoomIn" style="animation-delay: 0.4s;">
                                <label>Placa:</label>
                                <input required style="text-transform: uppercase;" value="<?= set_value('placa') ?>" class="form-control" name="placa" id="placa"/>
                                <span class="text-danger"><?= form_error("placa") ?></span>
                            </div>
                            <div class="col-md-1 form-group animated-panel zoomIn" style="animation-delay: 0.4s;">
                                <label>UF:</label>
                                <input style="text-transform: uppercase;" maxlength="2" value="<?= set_value('uf') ?>" class="form-control" name="uf"/>
                                <span class="text-danger"><?= form_error("uf") ?></span>
                            </div>
                            <div class="col-md-3 form-group animated-panel zoomIn" style="animation-delay: 0.4s;">
                                <label>Cidade:</label>
                                <input value="<?= set_value('cidade') ?>" class="form-control" name="cidade"  />
                                <span class="text-danger"><?= form_error("cidade") ?></span>
                            </div>
                            <div class="col-md-3 form-group animated-panel zoomIn" style="animation-delay: 0.4s;">
                                <label>Tipo:</label>
                                <div class="input-group">
                                    <select id="tipo" name="tipo" class="form-control">
                                        <option value="">Selecione...</option>
                                        <?php foreach ($tipos_veiculos as $tipo_veiculo): ?>
                                            <option <?= set_select("tipo", $tipo_veiculo->id) ?> value="<?= $tipo_veiculo->id ?>"><?= $tipo_veiculo->nome ?></option>
                                        <?php endforeach ?>
                                    </select>
                                </div>
                                <span class="text-danger"><?= form_error("tipo") ?></span>
                            </div>
                            <div class="col-md-3 form-group animated-panel zoomIn" style="animation-delay: 0.4s;">
                                <label>Fabricante:</label>
                                <input value="<?= set_value('fabricante') ?>" type="text" class="form-control" name="fabricante" />
                                <span class="text-danger"><?= form_error("fabricante") ?></span>
                            </div>
                            <div class="col-md-3 form-group animated-panel zoomIn" style="animation-delay: 0.4s;">
                                <label>Modelo:</label>
                                <input value="<?= set_value('modelo') ?>" type="text" class="form-control" name="modelo" />
                                <span class="text-danger"><?= form_error("modelo") ?></span>
                            </div>
                            <div class="col-md-3 form-group animated-panel zoomIn" style="animation-delay: 0.4s;">
                                <label>Cor:</label>
                                <input value="<?= set_value('cor') ?>" type="text" class="form-control" name="cor">
                                <span class="text-danger"><?= form_error("cor") ?></span>
                            </div>
                            <div class="col-md-3 form-group animated-panel zoomIn" style="animation-delay: 0.4s;">
                                 <button  data-ip-modal="#avatarModal" class="btn btn-primary btn-lg"><i class="fa fa-camera "></i> <span>Selecionar Imagem</span></button>
                            </div>

                            <div class="clearfix"></div>
                            <div class="form-group col-md-3 pull-right">
                                <button class="btn btn-primary btn-lg btn-block">Salvar</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php $this->load->view("morador/inc/footer"); ?>
</div>
<!-- Avatar Modal -->
<div class="ip-modal" id="avatarModal">
    <div class="ip-modal-dialog">
        <div class="ip-modal-content">
            <div class="ip-modal-header">
                <a class="ip-close" title="Close">&times;</a>
                <h4 class="ip-modal-title">Enviar imagem</h4>
            </div>
            <div class="ip-modal-body">
                <div class="btn btn-primary ip-upload">Enviar <input type="file" name="file" class="ip-file"></div>
                <button type="button" class="btn btn-primary ip-webcam">Webcam</button>
                <button type="button" class="btn btn-info ip-edit">Editar</button>
                <button type="button" class="btn btn-danger ip-delete">Excluir</button>

                <div class="alert ip-alert"></div>
                <div class="ip-info">Para editar a imagem, clique e arraste o mouse criando uma região. Solte o mouse e clique em "Salvar"</div>
                <div class="ip-preview"></div>
                <div class="ip-rotate">
                    <button type="button" class="btn btn-default ip-rotate-ccw" title="Rotate counter-clockwise"><i class="icon-ccw"></i></button>
                    <button type="button" class="btn btn-default ip-rotate-cw" title="Rotate clockwise"><i class="icon-cw"></i></button>
                </div>
                <div class="ip-progress">
                    <div class="text">Enviando</div>
                    <div class="progress progress-striped active"><div class="progress-bar"></div></div>
                </div>
            </div>
            <div class="ip-modal-footer">
                <div class="ip-actions">
                    <button type="button" class="btn btn-success ip-save">Salvar</button>
                    <button type="button" class="btn btn-primary ip-capture">Capturar</button>
                    <button type="button" class="btn btn-default ip-cancel">Cancelar</button>
                </div>
                <button type="button" class="btn btn-default ip-close">Fechar</button>
            </div>
        </div>
    </div>
</div>
<!-- end Modal -->

<?php $this->load->view("morador/inc/scripts_gerais") ?>
<!-- JavaScript CROPP -->
<script src="/assets/js/jquery.Jcrop.min.js"></script>
<script src="/assets/js/jquery.imgpicker.js"></script>
<!-- SELECT2 -->
<script src="/js/select2.min.js"></script>
<!-- MASK -->
<script src="/scripts/jquery.mask.min.js"></script>
<script>
    $(function() {
        // Avatar setup
        $('#avatarModal').imgPicker({
            url: '/server/upload_avatar_veiculo.php',
            aspectRatio: 1,
            data: {uniqid: '<?php echo uniqid() ?>'},
            deleteComplete: function() {
                $('#avatar2').attr('src', '/images/veiculo.png');
                this.modal('hide');
            },
            uploadSuccess: function(image) {
                // Calculate the default selection for the cropper
                var select = (image.width > image.height) ?
                [(image.width-image.height)/2, 0, image.height, image.height] :
                [0, (image.height-image.width)/2, image.width, image.width];      
                this.options.setSelect = select;
            },
            cropSuccess: function(image) {
                $('#avatar2').attr('src', "/imagens/veiculos/"+image.name );
                $('#url_imagem').attr('value', image.name );
                this.modal('hide');
            }
        });

        // Demo only
        $('.navbar-toggle').on('click',function(){$('.navbar-nav').toggleClass('navbar-collapse')});
        $(window).resize(function(e){if($(document).width()>=430)$('.navbar-nav').removeClass('navbar-collapse')});

        $("#tipo").select2({
            theme: "bootstrap",
            minimumInputLength: 2,
            minimumResultsForSearch: 0,
            language: {
                inputTooShort: function(args) {
                  // args.minimum is the minimum required length
                  // args.input is the user-typed text
                  return "Digite mais...";
              },
              inputTooLong: function(args) {
                  // args.maximum is the maximum allowed length
                  // args.input is the user-typed text
                  return "Excesso de caracteres";
              },
              errorLoading: function() {
                  return "Carregando...";
              },
              loadingMore: function() {
                  return "Carregando";
              },
              noResults: function() {
                  return "Nenhum resultado";
              },
              searching: function() {
                  return "Procurando...";
              },
              maximumSelected: function(args) {
                  // args.maximum is the maximum number of items the user may select
                  return "Erro ao carregar resultados";
              }
          },
          ajax: {
            url: "/veiculos/select2_listaTipos",
            dataType: "json",
            type: "post",
            data: function (params) {

                var queryParameters = {
                    term: params.term
                }
                return queryParameters;
            },
            processResults: function (data) {
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.nome,
                            id: item.id
                        }
                    })
                };
            }
        }
    });
        $("#placa").mask("SSS 0000");
    });
</script>
</body>
</html>