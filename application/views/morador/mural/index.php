<!DOCTYPE html>
<html>
<head>
    <!-- Page title -->
    <title>Residence Online</title>
    <?php $this->load->view("morador/inc/head_basico"); ?>
    <!-- CALENDÁRIO -->
    <link rel="stylesheet" href="/vendor/fullcalendar/dist/fullcalendar.print.css" media='print'/>
    <link rel="stylesheet" href="/vendor/fullcalendar/dist/fullcalendar.min.css" />
</head>
<body>
    <?php $this->load->view("morador/inc/menu_lateral") ?>

    <!-- Main Wrapper -->
    <div id="wrapper">

        <div class="normalheader ">
            <div class="hpanel">
                <div class="panel-body">
                    <h2 class="font-light m-b-xs">
                        Bem vind<?= ($usuario->sexo == "M" ? "o" : "a") ?>, <?= $usuario->nome ?>
                    </h2>
                    <small>Este é seu mural.</small>
                </div>
            </div>
        </div>
        <div class="content animate-panel">

            <?php if ($avisos != null): ?>
                <div class="row">
                    <h3 style="text-indent: 26px;">Últimos avisos</h3>
                    <?php foreach ($avisos as $aviso): ?>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="hpanel">
                                <div class="panel-body">
                                    <div class="stats-title pull-left">
                                        <h4>Até <?= date("d/m/Y", strtotime($aviso->data_validade)) ?></h4>
                                    </div>
                                    <div class="stats-icon pull-right">
                                        <i class="pe-7s-info fa-4x"></i>
                                    </div>
                                    <div class="m-t-xl">
                                        <h1 class="text-indigo" style="max-width: 100%;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;"><?= $aviso->titulo ?></h1>
                                        <small>
                                            <?= substr($aviso->descricao,0,150)  ?>
                                            <?php if (strlen($aviso->descricao) > 150): ?>
                                                ...
                                            <?php endif ?>
                                        </small>
                                        <button value="<?= $aviso->id ?>" class="btn btn-link ver pull-right">Ver mais <i class="fa fa-angle-double-right"></i></button>

                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endforeach ?>
                </div>
            <?php endif ?>

            <div class="row">
                <div class="col-lg-6">
                    <div class="hpanel">
                        <div class="panel-heading">
                            <h3>Reservas</h3>
                        </div>
                        <?php if ($reservas!= null): ?>
                            <div class="panel-body list">
                                <div class="pull-right">
                                    <a href="/areas" class="btn btn-xs btn-default">Ver Todas</a>
                                </div>
                                <div class="list-item-container">
                                    <?php foreach ($reservas as $reserva): ?>
                                        <div class="list-item">
                                            <h3 class="no-margins font-extra-bold text-indigo"><?= $reserva->nome_area ?></h3>
                                            <small>Reservado para <?= date("d/m/Y", strtotime($reserva->hora_inicio)) ?></small><br>
                                            <small><a href="#" onclick="return convidados(<?= $reserva->id ?>, this)"><?= $reserva->total_convidados ?> convidado<?= $reserva->total_convidados > 1 ? "s" : "" ?></a></small>
                                            <?= ($reserva->autorizado ? "<span class='label label-success pull-right '>Confirmado!</span>" : "<span class='label label-info pull-right '>Não confirmado.</span>") ?>
                                        </div>
                                    <?php endforeach ?>
                                </div>
                            </div>
                        <?php else: ?>
                            <div class="panel-body">
                                <h2 class="text-info text-center">Nenhuma reserva</h2>
                            </div>
                        <?php endif ?>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="hpanel">
                        <div class="panel-heading">
                            <h3>Comunique</h3>
                        </div>
                        <?php if ($comunique!= null): ?>
                            <div class="panel-body list">
                                <div class="pull-right">
                                    <a href="/comunique" class="btn btn-xs btn-default">Ver Todas</a>
                                </div>
                                <div class="table-responsive col-lg-12">
                                    <table class="table table-hover table-mailbox">
                                        <tbody>
                                            <?php foreach ($comunique as $sol): ?>
                                                <tr class="<?= (strtotime($sol->data_alteracao) > strtotime($this->session->userdata('ultimo_login'))) ? 'unread' : '' ; ?>">
                                                    <td class="">
                                                        <i class="fa fa-envelope-o "></i>
                                                    </td>
                                                    <td style="width: 20%;">
                                                        <a href="/comunique/ver/<?= $sol->id ?>">
                                                            <?php if ($sol->remetente == $this->session->userdata('id')): ?>
                                                                Você
                                                            <?php else: ?>
                                                                <?= $sol->nome_remetente ?>
                                                            <?php endif ?>
                                                        </a>                                                        
                                                    </td>
                                                    <td><a href="/comunique/ver/<?= $sol->id ?>"><?= $sol->titulo ?></a></td>
                                                    <td class="text-right mail-date">
                                                        <?php if ($sol->resolvida == 1): ?>
                                                            <span class="label label-success pull-left">Resolvida</span>
                                                        <?php endif ?>
                                                        <?= date('d/m/Y H:i', strtotime($sol->data_alteracao)) ?>
                                                    </td>
                                                </tr>
                                            <?php endforeach ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        <?php else: ?>
                            <div class="panel-body">
                                <h2 class="text-info text-center">Nenhuma comunicação</h2>
                            </div>
                        <?php endif ?>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-6">
                    <div class="hpanel">
                        <div class="panel-heading">
                            <h3>Fórum</h3>
                        </div>
                        <div class="panel-body list">
                            <div class="pull-right">
                                <a href="/forum" class="btn btn-xs btn-default">Ver Todas</a>
                            </div>
                            <div class="table-responsive project-list  col-lg-12">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>

                                            <th>Lugar</th>
                                            <th>Data</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        if($forum == null){
                                            echo "<tr><td colspan='2'><p class='text-info text-center'><strong>Nenhuma discussão</strong></p></td></tr>";
                                        }
                                        ?>
                                        <?php foreach ($forum as $f): ?>
                                            <tr>
                                                <td>
                                                    <strong><a href="/forum/ver/<?= $f->id ?>"><?= $f->titulo ?></a></strong>
                                                    <br/>
                                                    <small><i class="fa fa-user"></i> <?= $f->nome_usuario ?></small>
                                                </td>
                                                <td><i class="fa fa-clock-o"></i> Criado em <?= date("d/m/Y", strtotime($f->data_abertura)) ?></td>
                                            </tr>
                                        <?php endforeach ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="hpanel">
                        <div class="panel-heading">
                            <h3>Enquetes</h3>
                        </div>
                        <div class="panel-body list">
                            <div class="pull-right">
                                <a href="/enquetes" class="btn btn-xs btn-default">Ver Todas</a>
                            </div>
                            <div class="table-responsive project-list col-lg-12">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>

                                            <th>Título</th>
                                            <th>Data</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        if($enquetes == null){
                                            echo "<tr><td colspan='2'><p class='text-info text-center'><strong>Nenhuma enquete</strong></p></td></tr>";
                                        }
                                        ?>
                                        <?php foreach ($enquetes as $enquete): ?>
                                            <tr>
                                                <td>
                                                    <strong><a href="/enquetes/ver/<?= $enquete->id ?>"><?= $enquete->titulo ?></a></strong>
                                                    <br/>
                                                    <small><i class="fa fa-user"></i> <?= $enquete->nome_usuario ?></small>
                                                </td>
                                                <td><i class="fa fa-clock-o"></i> Criado em <?= date("d/m/Y", strtotime($enquete->data_abertura)) ?></td>
                                            </tr>
                                        <?php endforeach ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container_modal"></div>

        <?php $this->load->view("morador/inc/footer"); ?>

    </div>

    <?php $this->load->view("morador/inc/scripts_gerais") ?>
    <script type="text/javascript">
        function convidados(id, item){
            item = $(item);
            anterior = item.html();
            item.html("<i class='fa fa-cog fa-spin'></i>");
            $.post("/areas/convidados", {
                id: id
            }, function(result){
                $(".container_modal").html(result);
                item.html(anterior);
            });
            return false;
        }

        $(function(){
            $(".ver").click(function(){
                item = $(this);
                anterior = item.html();
                item.html("<i class='fa fa-cog fa-spin'></i>");
                $.post("/avisos/ver", {
                    id: $(this).val()
                }, function(result){
                    $(".container_modal").html(result);
                    item.html(anterior);
                });
            });
        });
    </script>

</body>
</html>