<?php
defined('BASEPATH') OR exit('Acesso negado');

class Residencia extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model("morador");
	}

	public function index(){
		if($this->morador->logado()){
			$dados = array(
				'menu_selecionado'=>'morador_minha_residencia',
			);
			$this->load->view("morador/residencia/index", $dados);
		}else{
			echo '<meta http-equiv="refresh" content="0; url=/" />';
		}
	}
}