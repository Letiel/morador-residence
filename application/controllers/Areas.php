<?php
defined('BASEPATH') OR exit('Acesso negado');

class Areas extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model("morador");
	}

	public function index(){
		if($this->morador->logado()){
			$areas = $this->morador->getAreas();
			$dados = array(
				'menu_selecionado'=>'morador_areas_comuns',
				'areas'=>$areas->result(),
				'total_areas'=>$areas->num_rows()
			);
			$this->load->view("morador/areas_comuns/index", $dados);
		}else{
			redirect("/");
		}
	}

	public function reservar(){
		if($this->morador->logado()){
			$area = (int) trim($this->input->post("id"));
			if(is_numeric($area)){
				$dados = array(
					'horarios'=>$this->morador->getHorariosArea($area)->result(),
					'area'=>$this->morador->getArea($area)->first_row(),
					'id_area'=>$area
				);
				$this->load->view("morador/areas_comuns/modals/reservar", $dados);
			}else
				redirect("/areas");
		}else{
			redirect("/");
		}
	}

	function ajax_reserva_area(){
		if($this->morador->logado()){
			$this->form_validation->set_rules("area", "Área", "required|is_numeric");
			$this->form_validation->set_rules("horario_inicio", "Data inicial", "required");
			$this->form_validation->set_rules("horario_final", "Data final", "required");

			if($this->form_validation->run()){
				$this->morador->reservar();
			}else{
				echo validation_errors();
			}
		}else{
			echo '<meta http-equiv="refresh" content="0; url=/" />';
		}
	}

	function ajax_cancelar_reserva(){
		if($this->morador->logado()){
			$this->form_validation->set_rules("id", "Id", "required|is_numeric");
			if($this->form_validation->run()){
				$this->morador->ajax_cancelar_reserva();
			}
		}else{
			echo '<meta http-equiv="refresh" content="0; url=/" />';
		}
	}

	function convidados(){
		if($this->morador->logado()){
			$this->form_validation->set_rules("id", "Id", "required|is_numeric");

			if($this->form_validation->run()){
				$convidados = $this->morador->getConvidados();
				$dados = array(
					'convidados'=>$convidados->result(),
					'total_convidados'=>$convidados->num_rows(),
					'id_reserva'=>$this->input->post("id"),
					'reserva'=>$this->morador->getReserva($this->input->post("id"))->first_row()
				);
				$this->load->view("morador/areas_comuns/modals/convidados", $dados);
			}
		}else{
			echo '<meta http-equiv="refresh" content="0; url=/" />';
		}
	}

	function ajax_cadastro_convidado(){
		if($this->morador->logado()){
			$this->form_validation->set_rules("reserva", "Reserva", "required|is_numeric");
			$this->form_validation->set_rules("nome", "Nome", "required");
			$this->form_validation->set_rules("rg", "RG", "");
			if($this->form_validation->run()){
				$this->morador->adicionar_convidado();
			}else{
				$this->form_validation->set_error_delimiters("<p class='text-danger text-center'>", "</p>");
				echo validation_errors();
			}
		}else{
			echo '<meta http-equiv="refresh" content="0; url=/" />';
		}
	}

	function ajax_atualizar_convidados(){
		if($this->morador->logado()){
			$this->form_validation->set_rules("id", "Id", "required|is_numeric");
			if($this->form_validation->run()){
				$convidados = $this->morador->getConvidados();
				if($convidados->num_rows() > 0){
					$convidados = $convidados->result();
					foreach ($convidados as $convidado){ ?>
					  <tr>
					    <td><?= $convidado->nome ?></td>
					    <td><?= $convidado->rg ?></td>
					    <td><button onclick="remover_convidado(<?= $convidado->id ?>)" class="btn btn-danger btn-outline remover_convidado" value="<?= $convidado->id ?>">Remover</button></td>
					  </tr>
					<?php
					}
				}else{
					echo "<tr><td colspan='3'><h2 class='text-info text-center'>Nenhum convidado.</h2></td></tr>";
				}
			}
		}else{
			echo '<meta http-equiv="refresh" content="0; url=/" />';
		}
	}

	function ajax_remover_convidado(){
		if($this->morador->logado()){
			$this->form_validation->set_rules("id", "Id", "required|is_numeric");
			if($this->form_validation->run()){
				$this->morador->ajax_remover_convidado();
			}
		}else{
			echo '<meta http-equiv="refresh" content="0; url=/" />';
		}
	}
}