<?php
defined('BASEPATH') OR exit('Acesso negado');

class Reservas extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model("morador");
	}

	public function index(){
		if ($this->morador->logado()) {
			$dados = array(
				'menu_selecionado' => 'morador_reservas',
				'reservas' => $this->morador->getReservas()->result()
				);
			$this->load->view("morador/reservas/index", $dados);
		}else{
			$this->load->view("login");
		}
	}	

	public function passadas(){
		if ($this->morador->logado()) {
			$this->load->library("pagination");

			$maximo = 10;
			$config['first_link'] = '<<';
			$config['last_link'] = '>>';
			$config['next_link'] = '>';
			$config['prev_link'] = '<';   
			$config['full_tag_open'] = '<nav class="paginacao"><ul class="pagination">';
			$config['full_tag_close'] = '</ul></nav>';
			$config['cur_tag_open'] = '<li class="active"><a href="">';
			$config['cur_tag_close'] = '</a></li>';
			$config['num_tag_open'] = '<li>';
			$config['num_tag_close'] = '</li>';
			$config['next_tag_open'] = '<li>';
			$config['next_tag_close'] = '</li>';
			$config['prev_tag_open'] = '<li>';
			$config['prev_tag_close'] = '</li>';
			$config['last_tag_open'] = '<li>';
			$config['last_tag_close'] = '</li>';
			$config['first_tag_open'] = '<li>';
			$config['first_tag_close'] = '</li>';
			$config['use_page_numbers'] = TRUE;
			$config['num_links'] = 3;
			$config['per_page'] = $maximo;

			$keyword = trim($this->input->get('k', TRUE));
			$config['enable_query_strings'] = TRUE;
			$config['query_string_segment'] = 'p';
			$config['page_query_string'] = TRUE;
			$config['base_url'] = "/reservas/passadas";
			if ($this->input->get('p')) {
				$sgm = (int) trim($this->input->get('p'));
				$inicio = $config['per_page'] * ($sgm - 1);
			} else {
				$inicio = 0;
			}

			$config['total_rows'] = $this->morador->getReservasPassadas(null, null)->num_rows();
			$this->pagination->initialize($config);

			$dados = array(
				'menu_selecionado'=>"morador_reservas",
				'reservas'=>$this->morador->getReservasPassadas($inicio, $maximo)->result(),
				'paginacao'=>$this->pagination->create_links()
				);
			$this->load->view("morador/reservas/passadas", $dados);
		}else{
			$this->load->view("login");
		}
	}
}