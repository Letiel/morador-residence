<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Veiculos extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model("morador");
	}

	public function index(){
		if(!$this->morador->logado()){
			redirect("/");
		}else{
			$veiculos = $this->morador->getVeiculos();
			$dados = array(
				'menu_selecionado'=>"morador_veiculos",
				'veiculos'=>$veiculos->result(),
				'total_veiculos'=>$veiculos->num_rows(),
			);
			$this->load->view("morador/veiculos/index", $dados);
		}
	}

	function ver(){
		if($this->morador->logado()){
			$id = $this->uri->segment(3);
			if(is_numeric($id)){
				$veiculo = $this->morador->getVeiculo($id);
				if($veiculo->first_row() != null){
					$dados = array(
						'menu_selecionado'=>'morador_veiculos',
						'veiculo'=>$veiculo->first_row()
					);
					$this->load->view("morador/veiculos/ver", $dados);
				}else
					redirect("/veiculos");
			}else{
				redirect("/veiculos");
			}
		}else{
			redirect("/");
		}
	}

	function cadastrar(){
		if($this->morador->logado()){
			$this->form_validation->set_rules("placa", "Placa", "required|addslashes|strtoupper");
			$this->form_validation->set_rules("uf", "UF", "addslashes");
			$this->form_validation->set_rules("cidade", "Cidade", "addslashes|strtolower|ucfirst");
			$this->form_validation->set_rules("tipo", "Tipo", "addslashes");
			$this->form_validation->set_rules("fabricante", "Fabricante", "addslashes|strtolower|ucfirst");
			$this->form_validation->set_rules("modelo", "Modelo", "addslashes|strtolower|ucfirst");
			$this->form_validation->set_rules("cor", "Cor", "addslashes");
			// $this->form_validation->set_rules("usuario_responsavel", "Usuário Responsável", "required|addslashes");
			// $this->form_validation->set_rules("residencia", "Residência", "addslashes");
			// $this->form_validation->set_rules("situacao", "Situação cadastral", "required|addslashes");
			// $this->form_validation->set_rules("obs_situacao", "Obs Situação", "addslashes|nl2br");

			$this->form_validation->set_error_delimiters('', '');

			if($this->form_validation->run()){
				$this->morador->cadastrar_veiculo();
			}


			$dados = array(
				'menu_selecionado'=>'morador_veiculos'
			);
			$this->load->view("morador/veiculos/cadastrar", $dados);
		}else{
			redirect("/");
		}
	}

	function select2_listaTipos(){
		echo json_encode($this->morador->select2_cadastros_gerais("VEICULOS")->result());
	}

	function select2_listaUsuarios(){
		echo json_encode($this->morador->select2_getMoradores()->result());
	}

	function ajax_cadastro_tipo(){
		if($this->morador->logado()){
			$this->form_validation->set_rules("nome", "Nome", "required|addslashes");
			if($this->form_validation->run()){
				$this->morador->ajax_cadastro_tipo_veiculo();
			}else{
				echo validation_errors();
			}
		}else{
			echo '<meta http-equiv="refresh" content="0; url=/" />';
		}
	}

	function editable(){ //ação do x-editable para alterar informações de veículos
		if($this->morador->logado()){
			$name = $_POST['name'];
			$value = $_POST['value'];
			$pk = $_POST['pk'];
			$result = array('success'=>false, 'msg'=>"Campo desconhecido");

			switch($name){
				case "placa":
					$this->form_validation->set_rules('value', 'Placa', 'required|addslashes|max_length[50]|strtoupper', array('required'=>'Não pode ficar vazio!'));
					break;
				case "uf":
					$this->form_validation->set_rules('value', 'UF', 'addslashes|max_length[2]|strtoupper');
					break;
				case "cidade":
					$this->form_validation->set_rules('value', 'Cidade', 'addslashes');
					break;
				case "tipo":
					$this->form_validation->set_rules('value', 'Tipo', 'is_numeric|addslashes', array("is_numeric"=>"Tipo inválido"));
					break;
				case "fabricante":
					$this->form_validation->set_rules('value', 'Fabricante', 'addslashes');
					break;
				case "modelo":
					$this->form_validation->set_rules('value', 'Modelo', 'addslashes');
					break;
				case "cor":
					$this->form_validation->set_rules('value', 'Cor', 'addslashes');
					break;
				case "usuario_responsavel":
					$this->form_validation->set_rules('value', 'Usuário Responsável', 'is_numeric|addslashes|required', array("is_numeric"=>"Usuário inválido."));
					break;
				case "residencia":
					$this->form_validation->set_rules('value', 'Residência', 'addslashes|required');
					break;
				case "situacao":
					$this->form_validation->set_rules('value', 'Situação', 'addslashes');
					break;
				case "obs_situacao":
					$this->form_validation->set_rules('value', 'Obs. situação', 'addslashes');
					break;
			}

			if($this->form_validation->run() == TRUE){
				$this->morador->editable_veiculo($name, $value, $pk);
				$result = array('success'=>true, 'newValue'=>$value);
			}else{
				$this->form_validation->set_error_delimiters('', '');
				$result = array('success'=>false, 'msg'=>validation_errors());
			}
		}else{
			$result = array('success'=>false, 'msg'=>'Você não está logado no sistema. <meta http-equiv="refresh" content="0; url=/" />');
		}
		echo json_encode($result);
	}

	function atualizar_foto(){
		if($this->morador->logado())
			$this->morador->atualizar_foto_veiculo();
		else
			echo '<meta http-equiv="refresh" content="0; url=/" />';
	}

	function deletar(){
		if($this->morador->logado())
			$this->morador->deletar_veiculo();
		else
			echo '<meta http-equiv="refresh" content="0; url=/" />';
	}
}
