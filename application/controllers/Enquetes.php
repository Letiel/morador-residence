<?php
defined('BASEPATH') OR exit('Acesso negado');

class Enquetes extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model("morador");
	}

	public function index(){
		if($this->morador->logado()){
			$this->load->library("pagination");

			$maximo = 10;
			$config['per_page'] = $maximo;
			$config['first_link'] = '<<';
			$config['last_link'] = '>>';
			$config['next_link'] = '>';
			$config['prev_link'] = '<';   
			$config['full_tag_open'] = '<nav class="paginacao"><ul class="pagination">';
			$config['full_tag_close'] = '</ul></nav>';
			$config['cur_tag_open'] = '<li class="active"><a href="">';
			$config['cur_tag_close'] = '</a></li>';
			$config['num_tag_open'] = '<li>';
			$config['num_tag_close'] = '</li>';
			$config['next_tag_open'] = '<li>';
			$config['next_tag_close'] = '</li>';
			$config['prev_tag_open'] = '<li>';
			$config['prev_tag_close'] = '</li>';
			$config['last_tag_open'] = '<li>';
			$config['last_tag_close'] = '</li>';
			$config['first_tag_open'] = '<li>';
			$config['first_tag_close'] = '</li>';
			$config['use_page_numbers'] = TRUE;
			$config['num_links'] = 3;

			$config['enable_query_strings'] = TRUE;
			$config['query_string_segment'] = 'p';
			$config['page_query_string'] = TRUE;
			$config['base_url'] = "/enquetes";
			if ($this->input->get('p')) {
			    $sgm = (int) trim($this->input->get('p'));
			    $inicio = $config['per_page'] * ($sgm - 1);
			} else {
			    $inicio = 0;
			}

			$config['total_rows'] = $this->morador->getEnquetes(null, null)->num_rows();
			$this->pagination->initialize($config);


			$enquetes = $this->morador->getEnquetes($inicio, $maximo);
			$dados = array(
				'total_enquetes'=>$config['total_rows'],
				'enquetes'=>$enquetes->result(),
				'menu_selecionado'=>'morador_enquetes',
				'paginacao'=> $this->pagination->create_links(),
			);
			$this->load->view("morador/enquetes/index", $dados);
		}else{
			redirect("/");
		}
	}

	function cadastrar(){
		if($this->morador->logado()){
			$this->form_validation->set_rules("titulo", "Título", "required");
			$this->form_validation->set_rules("opcoes[]", "Opções", "required", array("required"=>"Informe pelo menos duas opções."));
			$this->form_validation->set_rules("descricao", "Descrição", "nl2br");
			$this->form_validation->set_rules("data_validade", "Data de validade", "required");
			
			if($this->form_validation->run()){
				$this->morador->criar_enquete();
			}

			$dados = array(
				'menu_selecionado'=>'morador_enquetes',
			);
			$this->load->view("morador/enquetes/cadastrar", $dados);
		}else{
			redirect("/");
		}
	}

	function ver(){
		if($this->morador->logado()){
			$id = (int) trim($this->uri->segment(3));
			$opcoes = $this->morador->getOpcoesEnquete($id);
			$this->form_validation->set_rules('voto', 'Voto', 'trim|required', array("required"=>"Selecione uma opção."));
			if ($this->form_validation->run()) {
				$this->morador->votarEnquete();
			}

			$enquete = $this->morador->getEnquete($id);
			if($enquete->first_row() != null){
				$dados = array(
					'enquete'=>$enquete->first_row(),
					'opcoes'=>$opcoes->result(),
					'total_opcoes'=>$opcoes->num_rows(),
					'menu_selecionado'=>'morador_enquetes',
					'voto_enquete'=>$this->morador->getVotoEnquete($id)->first_row(),
					'votos'=>$this->morador->votosEnquete()->result()
				);
				$this->load->view("morador/enquetes/ver", $dados);
			}else
				redirect("/enquetes");
		}else{
			redirect("/");
		}
	}

	function alterar_validade(){
		if($this->morador->logado()){
			$this->form_validation->set_rules("id", "Id", "required|is_numeric|trim");
			$this->form_validation->set_rules("data_validade", "Data de validade", "required");
			if ($this->form_validation->run()) {
				$this->morador->alterar_validade_enquete();
			}
		}else{
			echo '<meta http-equiv="refresh" content="0; url=/" />';
		}
	}

	function desativar(){
		if($this->morador->logado()){
			$this->form_validation->set_rules("id", "Id", "required|is_numeric|trim");
			if ($this->form_validation->run()) {
				$this->morador->desativar_enquete();
			}
		}else{
			echo '<meta http-equiv="refresh" content="0; url=/" />';
		}
	}
}