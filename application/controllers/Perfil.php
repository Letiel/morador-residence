<?php
defined('BASEPATH') OR exit('Acesso negado');

class Perfil extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model("morador");
	}

	public function index(){
		if($this->morador->logado()){
			$dados = array(
				'menu_selecionado'=>'morador_perfil',
				'usuario'=>$this->morador->getMorador($this->session->userdata('id'))->first_row(),
				'veiculos'=>$this->morador->getVeiculos($this->session->userdata('id'))->result(),
				'localizacoes'=>$this->morador->getLocalizacoes()->result(),
				'boxes'=>$this->morador->getBoxes()->result()
			);
			$this->load->view("morador/perfil/index", $dados);
		}else{
			redirect("/");
		}
	}

	function editable(){ //ação do x-editable para alterar informações de moradores
		if($this->morador->logado()){

			function valid_date($date){    
			   $pattern = '/^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/|-|\.)(?:0?[1,3-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$/';

			    if(preg_match($pattern, $date) )
			        return true; 
			    else
			        return false;
			}

			$name = $_POST['name'];
			$value = $_POST['value'];
			$pk = $_POST['pk'];
			$result = array('success'=>false, 'msg'=>"Campo desconhecido");

			switch($name){
				case "nome":
					$this->form_validation->set_rules('value', 'Nome', 'required|addslashes|max_length[50]', array('required'=>'Não pode ficar vazio!', 'max_length'=>'Máximo de 50 caracteres no nome.'));
					break;
				case "cpf":
					$this->form_validation->set_rules('value', 'CPF', 'addslashes|max_length[14]', array('required'=>'Não pode ficar vazio!', 'max_length'=>'Máximo de 14 caracteres no cpf.'));
					break;
				case "rg":
					$this->form_validation->set_rules('value', 'RG', 'addslashes|max_length[20]', array('required'=>'Não pode ficar vazio!', 'max_length'=>'RG muito grande.'));
					break;
				case "usuario_tipo":
					$this->form_validation->set_rules('value', 'Tipo', 'required|addslashes', array('required'=>'Não pode ficar vazio!'));
					break;
				case "tipo":
					$this->form_validation->set_rules('value', 'Sub-tipo', 'required|addslashes', array('required'=>'Não pode ficar vazio!'));
					break;
				case "naturalidade":
					$this->form_validation->set_rules('value', 'Naturalidade', 'addslashes', array('required'=>'Não pode ficar vazio!'));
					break;
				case "sexo":
					$this->form_validation->set_rules('value', 'Sexo', 'required|addslashes', array('required'=>'Não pode ficar vazio!'));
					break;
				case "data_nascimento":
					$this->form_validation->set_rules('value', 'Data de nascimento', 'addslashes|valid_date', array('required'=>'Não pode ficar vazio!', 'valid_date'=>'Data inválida!'));
					break;
				case "email":
					$this->form_validation->set_rules('value', 'Email', 'addslashes', array('required'=>'Não pode ficar vazio!'));
					break;
				case "fone":
					$this->form_validation->set_rules('value', 'Fone', 'addslashes', array('required'=>'Não pode ficar vazio!'));
					break;
				case "cel1":
					$this->form_validation->set_rules('value', 'Cel 1', 'addslashes', array('required'=>'Não pode ficar vazio!'));
					break;
				case "cel2":
					$this->form_validation->set_rules('value', 'Cel 2', 'addslashes', array('required'=>'Não pode ficar vazio!'));
					break;
				case "endereco":
					$this->form_validation->set_rules('value', 'Endereço', 'addslashes', array('required'=>'Não pode ficar vazio!'));
					break;
				case "complemento":
					$this->form_validation->set_rules('value', 'Complemento', 'addslashes', array('required'=>'Não pode ficar vazio!'));
					break;
				case "bairro":
					$this->form_validation->set_rules('value', 'Bairro', 'addslashes', array('required'=>'Não pode ficar vazio!'));
					break;
				case "cidade":
					$this->form_validation->set_rules('value', 'Cidade', 'addslashes', array('required'=>'Não pode ficar vazio!'));
					break;
				case "uf":
					$this->form_validation->set_rules('value', 'UF', 'addslashes', array('required'=>'Não pode ficar vazio!'));
					break;
				case "cep":
					$this->form_validation->set_rules('value', 'CEP', 'addslashes', array('required'=>'Não pode ficar vazio!'));
					break;
				case "situacao":
					$this->form_validation->set_rules('value', 'Situação', 'addslashes', array('required'=>'Não pode ficar vazio!'));
					break;
				case "login":
					$this->form_validation->set_rules('value', 'Login', 'addslashes', array('required'=>'Não pode ficar vazio!'));
					break;
				case "senha":
					$this->form_validation->set_rules('value', 'Senha', 'required', array('required'=>'Não pode ficar vazio!'));
					$value = sha1($value);
					break;
				case "obs_situacao":
					$this->form_validation->set_rules('value', 'Obs situação', 'addslashes|nl2br', array('required'=>'Não pode ficar vazio!'));
					break;
				case "obs_administracao":
					$this->form_validation->set_rules('value', 'Obs administração', 'addslashes|nl2br', array('required'=>'Não pode ficar vazio!'));
					break;
				case "obs_portaria":
					$this->form_validation->set_rules('value', 'Obs portaria', 'addslashes|nl2br', array('required'=>'Não pode ficar vazio!'));
					break;
				case "localizacao":
					$this->form_validation->set_rules('value', 'Localização', 'required|addslashes', array('required'=>'Não pode ficar vazio!'));
					break;
				case "residencia":
					$this->form_validation->set_rules('value', 'Obs portaria', 'required|addslashes', array('required'=>'Não pode ficar vazio!'));
					break;
			}

			if($this->form_validation->run() == TRUE){
				$this->morador->editable_morador($name, $value, $pk);
				$result = array('success'=>true, 'newValue'=>$value);
			}else{
				$this->form_validation->set_error_delimiters('', '');
				$result = array('success'=>false, 'msg'=>validation_errors());
			}
		}else{
			$result = array('success'=>false, 'msg'=>'Você não está logado no sistema.');
		}
		echo json_encode($result);
	}

	function atualizar_foto(){
		if($this->morador->logado())
			$this->morador->atualizar_foto_morador();
		else
			echo '<meta http-equiv="refresh" content="0; url=/" />';
	}

	function editable_atualizar_subTipos(){ //tela de edição de cadastro de morador... x-editable de sub-tipo
		if($this->morador->logado())
			$this->morador->editable_atualizar_subTipos();
		else
			echo '<meta http-equiv="refresh" content="0; url=/" />';
	}

	function editable_atualizar_localizacoes(){ //tela de edição de cadastro de morador... x-editable de localizações
		if($this->morador->logado())
			$this->morador->editable_atualizar_localizacoes();
		else
			echo '<meta http-equiv="refresh" content="0; url=/" />';
	}

	function editable_atualizar_residencias(){ //tela de edição de cadastro de morador... x-editable de localizações
		if($this->morador->logado())
			$this->morador->editable_atualizar_residencias();
		else
			echo '<meta http-equiv="refresh" content="0; url=/" />';
	}

	function editable_pegar_residencia(){ //tela de edição de cadastro de morador... x-editable de residências
		if($this->morador->logado())
			$this->morador->editable_pegar_residencia();
		else
			echo '<meta http-equiv="refresh" content="0; url=/" />';
	}

	function ajax_alterar_login(){
		if($this->morador->logado()){
			$this->form_validation->set_rules('login', 'Login', 'trim|required|min_length[4]|is_unique[usuarios.login]');
			if($this->form_validation->run()){
				$this->morador->ajax_alterar_login();
			}else{
				echo validation_errors();
			}
		}else{
			echo '<meta http-equiv="refresh" content="0; url=/" />';
		}
	}

	function ajax_alterar_senha(){
		if($this->morador->logado()){
			$this->form_validation->set_rules('senha_antiga', 'Senha antiga', 'trim|required|sha1');
			$this->form_validation->set_rules('nova_senha', 'Nova senha', 'trim|required');
			$this->form_validation->set_rules('confirmacao_de_senha', 'Confirmacao de senha', 'trim|required|matches[nova_senha]');
			if($this->form_validation->run()){
				$this->morador->ajax_alterar_senha();
			}else{
				echo validation_errors();
			}
		}else{
			echo '<meta http-equiv="refresh" content="0; url=/" />';
		}
	}
}