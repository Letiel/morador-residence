<?php
defined('BASEPATH') OR exit('Acesso negado');

class Home extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model("morador");
	}

	public function index(){
		$this->form_validation->set_rules("login", "Login", "required|addslashes");
		$this->form_validation->set_rules("senha", "Senha", "required|addslashes");

		if($this->form_validation->run()){
			$this->morador->login();
		}else{
			$this->session->set_flashdata("mensagem_login", validation_errors());
		}
		if($this->morador->logado()){
			$dados = array(
				'menu_selecionado'=>"morador_mural",
				'usuario'=>$this->morador->getMorador($this->session->userdata('id'))->first_row(),
				'veiculos'=>$this->morador->getVeiculos($this->session->userdata('id'))->result(),
				'localizacoes'=>$this->morador->getLocalizacoes()->result(),
				'forum'=>$this->morador->getDiscussoesMural()->result(),
				'enquetes'=>$this->morador->getEnquetesMural()->result(),
				'reservas'=>$this->morador->getReservasMural()->result(),
				'comunique'=>$this->morador->getComuniqueMural()->result(),
				'avisos'=>$this->morador->getAvisos(0, 4)->result()
			);
			$this->load->view('morador/mural/index', $dados);
		}else{
			$this->load->view("login");
		}
	}

	public function sair(){
		$this->session->unset_userdata(array("id", "logado", "nome", "admin"));
		$this->index();
	}
}