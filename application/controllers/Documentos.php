<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Documentos extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model("morador");
	}

	public function index(){
		if(!$this->morador->logado(true)){
			$this->load->view("login");
		}else{
			$this->load->library("pagination");

			$maximo = 10;
			$config['per_page'] = $maximo;
			$config['first_link'] = '<<';
			$config['last_link'] = '>>';
			$config['next_link'] = '>';
			$config['prev_link'] = '<';   
			$config['full_tag_open'] = '<nav class="paginacao"><ul class="pagination">';
			$config['full_tag_close'] = '</ul></nav>';
			$config['cur_tag_open'] = '<li class="active"><a href="">';
			$config['cur_tag_close'] = '</a></li>';
			$config['num_tag_open'] = '<li>';
			$config['num_tag_close'] = '</li>';
			$config['next_tag_open'] = '<li>';
			$config['next_tag_close'] = '</li>';
			$config['prev_tag_open'] = '<li>';
			$config['prev_tag_close'] = '</li>';
			$config['last_tag_open'] = '<li>';
			$config['last_tag_close'] = '</li>';
			$config['first_tag_open'] = '<li>';
			$config['first_tag_close'] = '</li>';
			$config['use_page_numbers'] = TRUE;
			$config['num_links'] = 3;

			$keyword = trim($this->input->get('k', TRUE));
			$config['enable_query_strings'] = TRUE;
			$config['query_string_segment'] = 'p';
			$config['page_query_string'] = TRUE;
			$config['base_url'] = "/documentos?k=".$keyword;
			if ($this->input->get('p')) {
			    $sgm = (int) trim($this->input->get('p'));
			    $inicio = $config['per_page'] * ($sgm - 1);
			} else {
			    $inicio = 0;
			}

			$config['total_rows'] = $this->morador->getDocumentos(null, null, $keyword)->num_rows();
			$this->pagination->initialize($config);

			$dados = array(
				'menu_selecionado'=>"morador_documentos",
				'documentos'=>$this->morador->getDocumentos($inicio, $maximo, $keyword)->result(),
				'paginacao'=>$this->pagination->create_links()
			);
			$this->load->view("morador/documentos/index", $dados);
		}
	}

	function ver(){
		$documento = $this->morador->getDocumento(trim($this->uri->segment(3)))->first_row();
		if($documento == null){
			$this->session->set_flashdata("cadastro_documento", "<h4 class='text-danger'>Documento não encontrado.</h4>");
			redirect("/documentos");
		}
		$dados = array(
			'menu_selecionado'=>'morador_documentos',
			'documento'=>$documento
		);
		$this->load->view("morador/documentos/ver", $dados);
	}
}
