<?php
defined('BASEPATH') OR exit('Acesso negado');
// ini_set("display_errors", 0);

class Morador extends CI_Model{
	function __construct(){
		parent::__construct();
	}

	function logado($admin = false){
		if($this->session->userdata("logado")){

			$this->db->where("usuarios.id", $this->session->userdata('id'));
			$this->db->join("condominios", "condominios.cnpj = usuarios.cnpj");
			$this->db->select("usuarios.id, usuarios.nome, usuarios.cnpj, usuarios.usuario_tipo, usuarios.foto,  condominios.data_validade");
			$usuarios = $this->db->get("usuarios");
			if($usuarios->num_rows() == 1){
				$usuario = $usuarios->first_row();
				$this->session->set_userdata(array(
					'logado'=>true,
					'id'=>$usuario->id,
					'nome'=>$usuario->nome,
					'cnpj'=>$usuario->cnpj,
					'admin'=>($usuario->usuario_tipo == "COLABORADOR" ? true : false),
					'imagem'=>$usuario->foto
				));
			}
			if($admin){
				if($this->session->userdata("admin")){
					return true;
				}else{
					return false;
				}
			}else{
				return true;
			}
		}else{
			return false;
		}
	}

	function login(){
		$post = (object) $this->input->post();
		if($post->login != null && $post->senha != null){
			$this->db->where("(login LIKE '$post->login' OR email LIKE '$post->login')");
			$this->db->where("senha", sha1($post->senha));
			$this->db->join("condominios", "condominios.cnpj = usuarios.cnpj");
			$this->db->select("usuarios.id, usuarios.nome, usuarios.cnpj, usuarios.ultimo_login, usuarios.usuario_tipo, usuarios.foto,  condominios.data_validade");
			$usuarios = $this->db->get("usuarios");
			if($usuarios->num_rows() == 1){
				$usuario = $usuarios->first_row();
				if($usuario->data_validade >= date("Y-m-d H:i:s")){

					$this->session->set_userdata(array(
						'logado'=>true,
						'id'=>$usuario->id,
						'nome'=>$usuario->nome,
						'cnpj'=>$usuario->cnpj,
						'admin'=>($usuario->usuario_tipo == "COLABORADOR" ? true : false),
						'imagem'=>$usuario->foto,
						'ultimo_login'=> $usuario->ultimo_login,
						));

					$this->db->set("ultimo_login", date("Y-m-d H:i:s"));
					$this->db->where("id", $usuario->id);
					$this->db->update("usuarios");
					
				}else{
					$this->session->set_flashdata("mensagem_login", "<p>Não é possível entrar. <br /><small><small>Entre em contato com o administrador do sistema.</small></small></p>");
				}
			}else{
				$this->session->set_flashdata("mensagem_login", "Usuário ou senha inválidos.");
			}
		}
	}

	function enviarEmail($assunto, $para, $mensagem, $email = null){
		$this->load->library('email');

	    // Get full html:
		$body =
		'<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
		<html xmlns="http://www.w3.org/1999/xhtml">
		<head>
			<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
			<title>'.$assunto.'</title>
			<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
		</head>
		<body style="margin: 0; padding: 0;">
			<table border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td>
						<table align="center" border="0" cellpadding="0" cellspacing="0" width="600">
							<tr>
								<td align="center" bgcolor="#4051b5" style="padding: 50px 40px 40px 40px">
									<img src="'.base_url().'/images/ResidenceOnline.png" alt="Residence Online" width="50%"  style="display: block;" />
								</td>
							</tr>
							<tr>
								<td bgcolor="#ffffff" style="padding: 60px 30px 60px 30px;">
									<table border="0" cellpadding="0" cellspacing="0" width="100%">
										<tr>
											<td>
												'.$assunto.'
											</td>
										</tr>
										<tr>
											<td style="padding: 20px 0 30px 0;">
												<p>'.$mensagem.'</p>
											</td>
										</tr>
										<tr>
											<td style="padding: 20px 0 30px 0;">
												<p>Esta é uma mensagem automática. Não responda este email.</p>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td bgcolor="#4051b5">
									<table border="0" cellpadding="0" cellspacing="0" width="100%">
										<tr>
											<td bgcolor="#4051b5" style="padding: 30px 30px 30px 30px;">
												<table border="0" cellpadding="0" cellspacing="0" width="100%">
													<tr>
														<td width="75%" style="color:#ffffff;">
															<a style="color: #FFF; text-decoration: none; font-size: 20px;" href="'.base_url().'">
																&reg; Residence Online<br/>
															</a>
														</td>
														<td align="right">
															<table border="0" cellpadding="0" cellspacing="0">
																<tr>

																	<td style="font-size: 0; line-height: 0;" width="20">&nbsp;</td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</body>
		</html>';

		$endereco_remetente = (isset($email) ? $email : 'acessonaweb@acessonaweb.com.br');

		$config['protocol'] = 'sendmail';
		$config['mailpath'] = '/usr/sbin/sendmail';
		$config['charset'] = 'utf-8';
		$config['wordwrap'] = TRUE;
		$config['mailtype'] = "html";

		$this->email->initialize($config);

		$result = $this->email
		->from($endereco_remetente)
		->to($para)
		->subject($assunto)
		->message($body)
		->send();
	}

	function enviarNotificacao($id_usuario, $titulo, $mensagem, $show = "true"){
		$this->db->where("usuarios.id", $id_usuario);
		$usuario = $this->db->get("usuarios")->first_row();
		$url = 'https://fcm.googleapis.com/fcm/send';
		if($usuario != null)
			$token = $usuario->token_app;
		else
			$token = null;
		if($token != null && $token != ""){
			$fields = array (
				'registration_ids' => array (
					$token
					),

				'data' => array (
					"title" => $titulo,
					"message" => $mensagem,
					'show'=>$show
				)
			);
			$fields = json_encode ( $fields );

			$headers = array (
				'Authorization: key=' . "AAAAQrEcs8Y:APA91bHhfL5ue-C3J78jmS1JyQOzt06c9cbR9OcjTUYU-eAoAeqS-Q2Cy9BZrAwTeB5X06VDXdopwMZrxRqk2PJGZqtmZzRKph6k-9RIYiAH5_Zglbw5j0JYZd6PoYOj10f-SJIFNV0i",
				'Content-Type: application/json'
			);

			$ch = curl_init ();
			curl_setopt ( $ch, CURLOPT_URL, $url );
			curl_setopt ( $ch, CURLOPT_POST, true );
			curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
			curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
			curl_setopt ( $ch, CURLOPT_POSTFIELDS, $fields );

			$result = curl_exec ( $ch );
		    // echo $result;
			curl_close ( $ch );
		}
	}

	function enviarNotificacaoAdmin($id_usuario, $titulo, $mensagem, $show = "true"){
		$this->db->where("usuarios.id", $id_usuario);
		$usuario = $this->db->get("usuarios")->first_row();
		$url = 'https://fcm.googleapis.com/fcm/send';
		if($usuario != null)
			$token = $usuario->token_app_admin;
		else
			$token = null;
		if($token != null && $token != ""){
			$fields = array (
				'registration_ids' => array (
					$token
					),

				'data' => array (
					"title" => $titulo,
					"message" => $mensagem,
					'show'=>$show
					)
				);
			$fields = json_encode ( $fields );

			$headers = array (
				'Authorization: key=' . "AAAA6x7jgXk:APA91bEBaXTW0_i_8V7TgRznsCkgZ3zmvBtY7CHzD6dfBnQFkdss5IR7ZV2KD0nNxFDV2pGZqpoiujX5wBXQlYeYIxPIm6aPCPJbaBSaxpRMmT3XXtPavAtoKa0lY_2smIAoMXw36q-i",
				'Content-Type: application/json'
				);

			$ch = curl_init ();
			curl_setopt ( $ch, CURLOPT_URL, $url );
			curl_setopt ( $ch, CURLOPT_POST, true );
			curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
			curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
			curl_setopt ( $ch, CURLOPT_POSTFIELDS, $fields );

			$result = curl_exec ( $ch );
		    // echo $result;
			curl_close ( $ch );
		}
	}

	function cadastros_gerais($tipo, $relacao = NULL){
		$this->db->where("tipo", $tipo);
		if($relacao != NULL)
			$this->db->where("relacao", $relacao);
		$this->db->where("cnpj", $this->session->userdata("cnpj"));
		return $this->db->get("cadastros_gerais");
	}

	function ajax_cadastro_tipo_morador(){
		$post = $this->input->post();
		$post['cnpj'] = $this->session->userdata("cnpj");
		$post['operador_alteracao'] = $this->session->userdata("id");
		$this->db->insert("cadastros_gerais", $post);
	}

	function ajax_cadastro_localizacao(){
		$post = $this->input->post();
		$post['cnpj'] = $this->session->userdata("cnpj");
		$post['operador_alteracao'] = $this->session->userdata("id");
		$post['data_alteracao'] = date("Y-m-d H:i:s");
		$this->db->insert("localizacoes", $post);
	}

	function ajax_cadastro_residencia(){
		$post = $this->input->post();
		$post['cnpj'] = $this->session->userdata("cnpj");
		$post['operador_alteracao'] = $this->session->userdata("id");
		$post['data_alteracao'] = date("Y-m-d H:i:s");
		$this->db->insert("residencias", $post);
	}

	function select2_cadastros_gerais($tipo, $relacao = NULL){
		$post = $this->input->post();
		$this->db->order_by('nome', 'asc');
		$this->db->where("tipo", $tipo);
		$this->db->like("nome", $post['term']);
		if($relacao != NULL)
			$this->db->where("relacao", $relacao);
		$this->db->where("cnpj", $this->session->userdata("cnpj"));
		$this->db->select("id, nome");
		return $this->db->get("cadastros_gerais");
	}

	function select2_getMoradores(){
		$post = $this->input->post();
		$this->db->where("usuarios.cnpj", $this->session->userdata("cnpj"));
		// $this->db->where("cadastros_gerais.cnpj", $this->session->userdata("cnpj"));
		if($post['type'] != NULL && $post['type'] != "")
			$this->db->where("cadastros_gerais.tipo", $post['type']);
		$this->db->where("(usuarios.nome LIKE '%$post[term]%' OR residencias.nome LIKE '%$post[term]%' OR localizacoes.nome LIKE '%$post[term]%')");
		$this->db->join('residencias', 'residencias.id = usuarios.residencia', 'left');
		$this->db->join('localizacoes', 'residencias.localizacao = localizacoes.id', 'left');
		$this->db->join('cadastros_gerais', 'cadastros_gerais.id = usuarios.tipo', 'left');
		$this->db->select("usuarios.id, usuarios.nome, cadastros_gerais.nome as tipo, usuarios.situacao, IFNULL(residencias.nome, '') as residencia, IFNULL(localizacoes.nome, '') as localizacao");
		return $this->db->get("usuarios");
	}

	function select2_getLocalizacoes(){
		$post = $this->input->post();
		$this->db->where("localizacoes.cnpj", $this->session->userdata("cnpj"));
		$this->db->wher("(localizacoes.nome LIKE '%$post[term]%' OR localizacoes.descricao LIKE '%$post[term]%')");
		$this->db->select("localizacoes.id, localizacoes.nome");
		return $this->db->get("localizacoes");
	}

	function select2_getResidencias(){
		$post = $this->input->post();
		$this->db->where("residencias.cnpj", $this->session->userdata("cnpj"));
		if(isset($post['term'])){
			$this->db->where("(residencias.nome LIKE '%$post[term]%' OR localizacoes.nome LIKE '%$post[term]%')");
		}
		$this->db->join('localizacoes', 'residencias.localizacao = localizacoes.id', 'left');
		$this->db->select("residencias.id, residencias.nome, localizacoes.nome as localizacao");
		return $this->db->get("residencias");
	}

	// INÍCIO MURAL ----------------------------------------------------------------------------------------

	function getComuniqueMural(){
		$this->db->limit(5);
		$this->db->where("solicitacoes.cnpj", $this->session->userdata("cnpj"));
		$this->db->where("(solicitacoes.remetente = ".$this->session->userdata("id")." OR solicitacoes.destinatario = ".$this->session->userdata("id").")");
		$this->db->where("mensagens_solicitacoes.id IN (SELECT MAX(id) FROM mensagens_solicitacoes WHERE mensagens_solicitacoes.solicitacao = solicitacoes.id)");
		// $this->db->where("solicitacoes.resolvida", 0);
		$this->db->join("mensagens_solicitacoes", "mensagens_solicitacoes.solicitacao = solicitacoes.id", "left");
		$this->db->join("usuarios", "mensagens_solicitacoes.remetente = usuarios.id", "left");
		$this->db->select("solicitacoes.*, mensagens_solicitacoes.mensagem, usuarios.nome as nome_remetente, mensagens_solicitacoes.data_alteracao as ultima_mensagem");
		$this->db->order_by("mensagens_solicitacoes.id DESC");
		$this->db->group_by("solicitacoes.id");
		return $this->db->get("solicitacoes");
		// $this->db->get("solicitacoes");
		// echo $this->db->last_query();
		// exit();
	}

	function getReservasMural(){
		$this->db->limit(5);
		$this->db->where("reservas.usuario", $this->session->userdata("id"));
		$this->db->where("reservas.cnpj", $this->session->userdata("cnpj"));
		$this->db->where("reservas.hora_inicio > '".date("Y-m-d H:i:s")."'");
		$this->db->join("areas", "areas.id = reservas.area");
		$this->db->join("convidados_reservas", "reservas.id = convidados_reservas.reserva", "LEFT");
		$this->db->select("reservas.*, areas.nome as nome_area, COUNT(convidados_reservas.id) as total_convidados");
		$this->db->order_by("reservas.id", "DESC");
		$this->db->group_by("reservas.id");
		return $this->db->get("reservas");
	}

	function getEnquetesMural(){
		$this->db->limit(5);
		$this->db->where("enquetes.cnpj", $this->session->userdata("cnpj"));
		$this->db->where("enquetes.data_validade > '".date("Y-m-d H:i:s")."'");
		$this->db->where("(enquetes.ativado = 1 OR enquetes.usuario = ".$this->session->userdata("id").")");
		$this->db->join("usuarios", "usuarios.id = enquetes.usuario", 'left');

		$this->db->join("opcoes_enquetes", "enquetes.id = opcoes_enquetes.enquete", 'left');
		$this->db->join("votos_enquetes", "opcoes_enquetes.id = votos_enquetes.voto", 'left');

		$this->db->select("enquetes.*, usuarios.nome as nome_usuario, COUNT(votos_enquetes.id) as total_votos");
		$this->db->group_by("enquetes.id");
		$this->db->order_by("enquetes.data_validade DESC, enquetes.ativado DESC, enquetes.id DESC");
		return $this->db->get("enquetes");
	}

	function getDiscussoesMural(){
		$this->db->limit(5);
		$this->db->where("discussoes.cnpj", $this->session->userdata("cnpj"));
		$this->db->where("discussoes.ativado", 1);
		$this->db->join("usuarios", "usuarios.id = discussoes.usuario", "left");
		$this->db->select("discussoes.*, usuarios.nome as nome_usuario");
		$this->db->order_by("id", "DESC");
		return $this->db->get("discussoes_forum as discussoes");
	}
	// FIM MURAL -------------------------------------------------------------------------------------------
	// INÍCIO CRUD DE MORADORES ----------------------------------------------------------------------------
	
	function getMoradores($inicio = null, $offset = null, $keyword){
		// if($inicio != null && $offset != null)
		$this->db->limit($offset, $inicio);
		$this->db->where("(usuarios.nome LIKE '%$post[term]%' OR cadastros_gerais.nome LIKE '%$post[term]%')");
		$this->db->where("usuarios.cnpj", $this->session->userdata("cnpj"));
		$this->db->where("cadastros_gerais.cnpj", $this->session->userdata("cnpj"));
		$this->db->join('cadastros_gerais', 'cadastros_gerais.id = usuarios.tipo', 'left');
		$this->db->select("usuarios.id, usuarios.nome, cadastros_gerais.nome as tipo, usuarios.situacao");
		return $this->db->get("usuarios");
	}

	function cadastrar_morador(){
		if($this->logado()){
			$post = $this->input->post();
			$post['operador_alteracao'] = $this->session->userdata("id");
			$post['cnpj'] = $this->session->userdata("cnpj");
			$post['data_alteracao'] = date("Y-m-d H:i:s");
			if($post['login'] == "")
				$post['login'] = null;
			if($this->db->insert("usuarios", $post)){
				$this->session->set_flashdata("mensagem_cadastro_morador", "Morador cadastrado com sucesso!");
				redirect("/moradores");
			}
		}
	}

	function getMorador($id = NULL){
		$this->db->where("usuarios.cnpj", $this->session->userdata("cnpj"));
		$this->db->where("usuarios.id", $id);
		$this->db->join("residencias", "residencias.id = usuarios.residencia", "left");
		$this->db->join("localizacoes", "localizacoes.id = residencias.localizacao", "left");
		$this->db->select("usuarios.*, residencias.nome as nome_residencia, localizacoes.nome as nome_localizacao");
		return $this->db->get("usuarios");
	}

	function getTiposPorUsuario(){ //pegando para preencher os sub-tipos de usuário no form de edição de morador
		$post = $this->input->post();
		$this->db->where("cadastros_gerais.cnpj", $this->session->userdata("cnpj"));
		$this->db->where("cadastros_gerais.tipo", $post['tipo']);
		$this->db->select("id as value, nome as text");
		$subTipos = $this->db->get("cadastros_gerais");
		$total = $subtipos->num_rows();
		$subTipos = $subTipos->result();
		if($total == 0){
			echo "{\"value\": '0', \"text\": 'Nenhum cadastrado.'},";
		}else{
			echo "[";
			foreach($subTipos as $subTipo){
				echo "{\"value\": '$subTipo->value', \"text\": '$subTipo->text'},";
			}
			echo "]";
		}
	}

	function getLocalizacoes(){
		$this->db->where("cnpj", $this->session->userdata("cnpj"));
		return $this->db->get("localizacoes");
	}

	function ajax_atualizar_subTipos(){
		$post = $this->input->post();
		$this->db->where("tipo", $post['tipo']);
		$this->db->where("cnpj", $this->session->userdata("cnpj"));
		$this->db->select("id, nome");
		$tipos = $this->db->get("cadastros_gerais");
		$total = $tipos->num_rows();
		$tipos = $tipos->result();
		if($total == 0){
			echo "<option value=''>Nenhum resultado encontrado</option>";
		}else{
			foreach($tipos as $tipo){
				echo "<option ".($tipo->id == $post['selecionado'] ? 'selected' : '')." value='$tipo->id'>$tipo->nome</option>";
			}
		}
	}

	function ajax_atualizar_localizacoes(){ //retorna novas residencias sempre que o campo localização é alterado
		$post = $this->input->post();
		$this->db->where("cnpj", $this->session->userdata("cnpj"));
		$this->db->select("id, nome");
		$localizacoes = $this->db->get("localizacoes");

		if($localizacoes->num_rows() > 0){
			$localizacoes = $localizacoes->result();
			foreach($localizacoes as $localizacao){
				echo "<option value='$localizacao->id'>$localizacao->nome</option>";
			}
		}else{
			echo "<option disabled value=''>Nenhuma encontrada</option>";
		}
	}

	function ajax_atualizar_residencias(){ //retorna novas residencias sempre que o campo localização é alterado
		$post = $this->input->post();
		$this->db->where("localizacao", $post['localizacao']);
		$this->db->where("cnpj", $this->session->userdata("cnpj"));
		$this->db->select("id, nome");
		$residencias = $this->db->get("residencias");

		if($residencias->num_rows() > 0){
			$residencias = $residencias->result();
			foreach($residencias as $residencia){
				echo "<option ".($residencia->id == $post['selecionado'] ? 'selected' : '')." value='$residencia->id'>$residencia->nome</option>";
			}
		}else{
			echo "<option disabled value=''>Nenhuma encontrada</option>";
		}
	}

	// FIM CRUD MORADORES    -------------------------------------------------------------------------
	// INÍCIO ALTERAR PERFIL -------------------------------------------------------------------------

	function editable_morador($name, $value, $pk){
		$this->db->where("id", $pk);
		$this->db->where("cnpj", $this->session->userdata("cnpj"));
		$array = array(
			"$name"=>"$value"
			);
		$this->db->update("usuarios", $array);
	}

	function editable_atualizar_subTipos(){ //retorna novos sub-tipos sempre que o campo tipos é alterado
		$post = $this->input->post();
		$this->db->where("tipo", $post['tipo']);
		$this->db->where("cnpj", $this->session->userdata("cnpj"));
		$this->db->select("id as value, nome as text");
		$tipos = $this->db->get("cadastros_gerais");
		$total = $tipos->num_rows();
		$tipos = $tipos->result();
		if($total == 0){
			echo "{\"value\": \"\", \"text\": \"Nenhum resultado encontrado\"}";
		}else{
			$i = 0;
			echo "[";
			$print = "";
			foreach($tipos as $tipo){
				$print = "{\"value\": \"$tipo->value\", \"text\": \"$tipo->text\"}";
				if($i < $total-1)
					$print .=",";
				echo $print;
				$i++;
			}
			echo "]";
		}
	}

	function editable_atualizar_localizacoes(){
		$post = $this->input->post();
		$this->db->where("cnpj", $this->session->userdata("cnpj"));
		$this->db->select("id as value, nome as text");
		$localizacoes = $this->db->get("localizacoes");
		$total = $localizacoes->num_rows();
		$localizacoes = $localizacoes->result();
		if($total == 0){
			echo "{\"value\": \"\", \"text\": \"Nenhum resultado encontrado\"}";
		}else{
			$i = 0;
			echo "[";
			$print = "";
			foreach($localizacoes as $localizacao){
				$print = "{\"value\": \"$localizacao->value\", \"text\": \"$localizacao->text\"}";
				if($i < $total-1)
					$print .=",";
				echo $print;
				$i++;
			}
			echo "]";
		}
	}

	function editable_atualizar_residencias(){ //retorna novas residências para o x-editable sempre que o campo localizações é alterado
		$post = $this->input->post();
		$this->db->where("localizacao", $post['localizacao']);
		$this->db->where("cnpj", $this->session->userdata("cnpj"));
		$this->db->select("id as value, nome as text");
		$residencias = $this->db->get("residencias");
		$total = $residencias->num_rows();
		$residencias = $residencias->result();
		if($total == 0){
			echo "{\"value\": \"\", \"text\": \"Nenhum resultado encontrado\"}";
		}else{
			$i = 0;
			echo "[";
			$print = "";
			foreach($residencias as $residencia){
				$print = "{\"value\": \"$residencia->value\", \"text\": \"$residencia->text\"}";
				if($i < $total-1)
					$print .=",";
				echo $print;
				$i++;
			}
			echo "]";
		}
	}

	function atualizar_foto_morador(){
		$post = $this->input->post();
		$this->db->start_cache();
		$this->db->where("usuarios.id", $post['id']);
		$this->db->where("usuarios.cnpj", $this->session->userdata("cnpj"));
		$this->db->select("usuarios.foto, usuarios.id");
		$usuario = $this->db->get("usuarios");
		$this->db->stop_cache();
		$this->db->flush_cache();
		$total = $usuario->num_rows();
		if($total == 1){
			$usuario = $usuario->first_row();
			$this->db->where("usuarios.id", $post['id']);
			$this->db->where("usuarios.cnpj", $this->session->userdata("cnpj"));
			$this->db->update("usuarios", array('foto'=>$post['foto']));
		}
	}

	function deletar_morador(){
		$post = $this->input->post();
		if(is_numeric($post['id'])){
			$this->db->start_cache();
			$this->db->where("id", $post['id']);
			$this->db->where("cnpj", $this->session->userdata("cnpj"));
			$this->db->delete("usuarios");
			$this->db->stop_cache();
			$this->db->flush_cache();
			$this->session->set_flashdata("mensagem_cadastro_morador", "Removido com sucesso");
		}
	}

	function ajax_alterar_login(){
		$this->db->where("id", $this->session->userdata("id"));
		$this->db->where("cnpj", $this->session->userdata("cnpj"));
		$dados = array("login"=>$this->input->post("login"));
		$this->db->update("usuarios", $dados);
		$this->session->set_flashdata("mensagem_perfil", "Login alterado com sucesso.");
	}

	function ajax_alterar_senha(){
		$this->db->start_cache();
		$this->db->where("id", $this->session->userdata("id"));
		$this->db->where("cnpj", $this->session->userdata("cnpj"));
		$usuario = $this->db->get("usuarios")->first_row();
		if($usuario->senha != $this->input->post("senha_antiga")){
			echo "<p>Senha antiga incorreta!</p>";
		}else{
			$this->db->stop_cache();
			$this->db->flush_cache();
			$this->db->where("id", $this->session->userdata("id"));
			$this->db->where("cnpj", $this->session->userdata("cnpj"));
			$dados = array("senha"=>sha1($this->input->post("nova_senha")));
			$this->db->update("usuarios", $dados);
			$this->session->set_flashdata("mensagem_perfil", "Senha alterada com sucesso.");
		}
	}

	function getBoxes(){
		$this->db->where("boxes.cnpj", $this->session->userdata("cnpj"));
		$this->db->where("usuarios.id", $this->session->userdata("id"));
		
		$this->db->join("residencias","boxes.residencia = residencias.id");
		$this->db->join("usuarios", "residencias.id = usuarios.residencia");
		$this->db->select("boxes.*");
		return $this->db->get("boxes");
	}

	// FIM ALTERAR PERFIL ----------------------------------------------------------------------------

	// INÍCIO CRUD DE SOLICITACOES ------------------------------------------------------------------------
	// function getSolicitacoes($tipo = NULL, $inicio = NULL, $offset = NULL, $keyword = NULL){
	// 	if($tipo != NULL){
	// 		$this->db->limit($offset, $inicio);
	// 		$this->db->where("solicitacoes.cnpj", $this->session->userdata("cnpj"));
	// 		$this->db->where("$tipo.cnpj", $this->session->userdata("cnpj"));
	// 		$this->db->where("participantes_solicitacoes.usuario", $this->session->userdata("id"));
	// 		// $this->db->where($tipo, $this->session->userdata("id"));
	// 		$this->db->join("participantes_solicitacoes", "participantes_solicitacoes.solicitacao = solicitacoes.id");
	// 		$this->db->join("usuarios as remetente", "remetente.id = solicitacoes.remetente", "left");
	// 		$this->db->join("usuarios as destinatario", "destinatario.id = solicitacoes.destinatario", "left");
	// 		$this->db->order_by("id", "DESC");
	// 		$this->db->select("solicitacoes.*, destinatario.nome as nome_destinatario, remetente.nome as nome_remetente");
	// 		return $this->db->get("solicitacoes");
	// 	}
	// }

	function getSolicitacoes($inicio = NULL, $offset = NULL, $keyword = NULL){
		$this->db->limit($offset, $inicio);
		$this->db->order_by("solicitacoes.data_alteracao", 'desc');
		$this->db->join('participantes_solicitacoes', "participantes_solicitacoes.solicitacao = solicitacoes.id");
		$this->db->join('usuarios', "solicitacoes.remetente = usuarios.id");
		$this->db->where("participantes_solicitacoes.usuario", $this->session->userdata('id'));
		$this->db->where("solicitacoes.cnpj", $this->session->userdata('cnpj'));
		$this->db->select("solicitacoes.* , usuarios.nome AS nome_remetente");
		return $this->db->get("solicitacoes");
	}

	function getSolicitacao($id = NULL){
		$this->session->unset_userdata("ultimaMensagem");
		if($id != NULL){
			$this->db->where("solicitacoes.cnpj", $this->session->userdata("cnpj"));
			$this->db->where("remetente.cnpj", $this->session->userdata("cnpj"));
			// $this->db->where("destinatario.cnpj", $this->session->userdata("cnpj"));
			// $this->db->where("(destinatario.id = ".$this->session->userdata("id")." OR remetente.id = ".$this->session->userdata("id").")");
			$this->db->where($this->session->userdata('id')." IN (SELECT usuario FROM participantes_solicitacoes WHERE usuario = ".$this->session->userdata('id')." AND participantes_solicitacoes.solicitacao = $id)");
			$this->db->where("solicitacoes.id", $id);
			$this->db->join("usuarios as remetente", "remetente.id = solicitacoes.remetente", "left");
			$this->db->join("usuarios as destinatario", "destinatario.id = solicitacoes.destinatario", "left");
			$this->db->join("residencias as residencia_remetente", "remetente.residencia = residencia_remetente.id");
			$this->db->join("residencias as residencia_destinatario", "destinatario.residencia = residencia_destinatario.id");

			$this->db->join("localizacoes as localizacao_remetente", "remetente.localizacao = localizacao_remetente.id");
			$this->db->join("localizacoes as localizacao_destinatario", "destinatario.localizacao = localizacao_destinatario.id");

			$this->db->select("solicitacoes.*, remetente.nome as nome_remetente, remetente.foto as foto_remetente, destinatario.nome as nome_destinatario, destinatario.foto as foto_destinatario, residencia_remetente.nome as residencia_remetente, localizacao_remetente.nome as localizacao_remetente, residencia_destinatario.nome as residencia_destinatario, localizacao_destinatario.nome as localizacao_destinatario");
			return $this->db->get("solicitacoes");
		}
	}

	function getParticipantesSolicitacao($id = NULL){
		$this->db->where("participantes_solicitacoes.cnpj", $this->session->userdata("cnpj"));
		$this->db->where("participantes_solicitacoes.solicitacao", $id);
		$this->db->join("usuarios", "usuarios.id = participantes_solicitacoes.usuario", 'left');
		$this->db->join("residencias", "usuarios.residencia = residencias.id", 'left');
		$this->db->join("localizacoes", "residencias.localizacao = localizacoes.id", 'left');
		$this->db->join("solicitacoes", "participantes_solicitacoes.solicitacao = solicitacoes.id");
		$this->db->select("usuarios.id, usuarios.nome, usuarios.foto, localizacoes.nome as localizacao_participante, residencias.nome as residencia_participante");
		return $this->db->get("participantes_solicitacoes");
	}

	function testeParticipante($id){
		$this->db->start_cache();
		$this->db->where("usuario", $this->session->userdata("id"));
		$this->db->where("cnpj", $this->session->userdata("cnpj"));
		$this->db->where("solicitacao", $id);
		return $this->db->get("participantes_solicitacoes");
	}

	function getMensagensServerSent($id = NULL){
		$this->db->stop_cache();
		$this->db->flush_cache();

		$ultimaMensagem = $this->session->userdata("ultimaMensagem");
		$this->db->where("mensagens_solicitacoes.cnpj", $this->session->userdata("cnpj"));
		$this->db->where("mensagens_solicitacoes.solicitacao", $id);
		$this->db->where("mensagens_solicitacoes.data_alteracao > '".$ultimaMensagem."'");
		$this->db->join("usuarios as remetente", "remetente.id = mensagens_solicitacoes.remetente");
		$this->db->join("usuarios as destinatario", "destinatario.id = mensagens_solicitacoes.destinatario", 'left');
		$this->db->select("mensagens_solicitacoes.*, remetente.nome as nome_remetente, destinatario.nome as nome_destinatario, remetente.foto as foto_remetente, destinatario.foto as foto_destinatario");
		$this->db->order_by("id", "ASC");
		return $this->db->get("mensagens_solicitacoes");
	}

	function getMensagensSolicitacao($id = NULL){
		$this->db->where("mensagens_solicitacoes.cnpj", $this->session->userdata("cnpj"));
		$this->db->where("mensagens_solicitacoes.solicitacao", $id);
		$this->db->join("usuarios as remetente", "remetente.id = mensagens_solicitacoes.remetente");
		// $this->db->join("usuarios as destinatario", "destinatario.id = mensagens_solicitacoes.destinatario");
		$this->db->select("mensagens_solicitacoes.*, remetente.nome as nome_remetente, remetente.foto as foto_remetente");
		$this->db->order_by("data_alteracao", "ASC");
		return $this->db->get("mensagens_solicitacoes");
	}

	function enviarMensagemSolicitacao(){
		$post = $this->input->post();
		$this->db->start_cache();
		$this->db->where("cnpj", $this->session->userdata("cnpj"));
		$this->db->where("id", $post['id']);
		$solicitacao = $this->db->get("solicitacoes");
		$this->db->stop_cache();
		$this->db->flush_cache();
		if($solicitacao->num_rows() == 1){
			$solicitacao = $solicitacao->first_row();
			if($solicitacao->remetente == $this->session->userdata("id")){
				$destinatario = $solicitacao->destinatario;
			}else if($solicitacao->destinatario == $this->session->userdata("id")){
				$destinatario = $solicitacao->remetente;
			}else{
				$destinarario = NULL;
			}
			if($solicitacao->destinatario != NULL){
				$dados = array(
					'solicitacao'=>$post['id'],
					'mensagem'=>$post['mensagem'],
					'cnpj'=>$this->session->userdata("cnpj"),
					'remetente'=>$this->session->userdata("id"),
					// 'destinatario'=>$destinatario,
					"operador_alteracao"=>$this->session->userdata("id"),
					'data_alteracao'=>date("Y-m-d H:i:s"),
				);
				if($this->db->insert("mensagens_solicitacoes", $dados)){
					$this->db->start_cache();
					$this->db->where("usuarios.cnpj", $this->session->userdata("cnpj"));
					$this->db->where("usuarios.id IN (SELECT usuario FROM participantes_solicitacoes WHERE solicitacao = $post[id] AND usuario != ".$this->session->userdata('id').")");
					$usuarios = $this->db->get("usuarios")->result();
					$this->db->stop_cache();
					$this->db->flush_cache();

					$this->db->start_cache();
					$this->db->where("usuarios.cnpj", $this->session->userdata("cnpj"));
					$this->db->where("usuarios.id", $this->session->userdata("id"));
					$remetente = $this->db->get("usuarios")->first_row();
					$this->db->stop_cache();
					$this->db->flush_cache();

					foreach ($usuarios as $usuario) {
						if($usuario->token_app != null && $usuario->token_app != ""){
							if($usuario->id != $this->session->userdata("id"))
								$this->enviarNotificacao($usuario->id, $remetente->nome, $post['mensagem'], 'false');
						}

						if($usuario->token_app_admin != null && $usuario->token_app_admin != ""){
							if($usuario->id != $this->session->userdata("id"))
								$this->enviarNotificacaoAdmin($usuario->id, $remetente->nome, $post['mensagem'], 'false');
						}	
					}
				}
			}
		}
	}

	function adicionarParticipanteComunique(){
		$post = $this->input->post();
		$this->db->where("participantes_solicitacoes.solicitacao", $post['id']);
		$this->db->where("participantes_solicitacoes.cnpj", $this->session->userdata("cnpj"));
		$this->db->where("participantes_solicitacoes.usuario", $post['usuario']);
		$solicitacoes = $this->db->get("participantes_solicitacoes");
		if($solicitacoes->num_rows() == 0){
			$dados = array(
				"usuario"=>$post['usuario'],
				"solicitacao"=>$post['id'],
				"cnpj"=>$this->session->userdata("cnpj")
			);
			$this->db->insert("participantes_solicitacoes", $dados);
		}else{
			echo "<p class='text-danger'>Usuário já está na comunicação.</p>";
		}
	}

	function removerParticipanteComunique(){
		$post = $this->input->post();
		$this->db->where("participantes_solicitacoes.solicitacao", $post['id']);
		$this->db->where("participantes_solicitacoes.cnpj", $this->session->userdata("cnpj"));
		$this->db->where("participantes_solicitacoes.usuario", $post['usuario']);
		$solicitacoes = $this->db->get("participantes_solicitacoes");
		if($solicitacoes->num_rows() != 0){
			$this->db->query("DELETE FROM participantes_solicitacoes WHERE solicitacao = $post[id] AND usuario = $post[usuario]");
		}
	}

	function ajax_criar_comunique(){
		$post = $this->input->post();
		$dados = array(
			'cnpj'=>$this->session->userdata("cnpj"),
			'operador_alteracao'=>$this->session->userdata("id"),
			'remetente'=>$this->session->userdata("id"),
			'destinatario'=>$post['destinatario'],
			'titulo'=>$post['titulo'],
			'descricao'=>$post['descricao'],
			'data_alteracao'=>date("Y-m-d H:i:s")
		);
		$this->db->insert("solicitacoes", $dados);
		$id = $this->db->insert_id();

		$this->db->start_cache();
		$this->db->insert("participantes_solicitacoes", array("usuario"=>$this->session->userdata("id"),"cnpj"=>$this->session->userdata("cnpj"), "solicitacao"=>$id));
		$this->db->flush_cache();
		$this->db->stop_cache();

		$this->db->start_cache();
		$this->db->insert("participantes_solicitacoes", array("usuario"=>$dados['destinatario'],"cnpj"=>$this->session->userdata("cnpj"), "solicitacao"=>$id));
		$this->db->flush_cache();
		$this->db->stop_cache();

		$this->db->start_cache();
		$this->db->where("usuarios.id", $dados['destinatario']);
		$this->db->where("usuarios.cnpj", $this->session->userdata("cnpj"));
		$this->db->select("usuarios.id, usuarios.email, usuarios.nome, usuarios.token_app, usuarios.token_app_admin");
		$destinatario = $this->db->get("usuarios")->first_row();
		$this->db->flush_cache();
		$this->db->stop_cache();
		if($destinatario != null){
			if($destinatario->email != null && trim($destinatario->email) != ""){
				$mensagem = "
				<p>Uma nova comunicação com você foi aberta no <b><a style='text-decoration: none;' href='".base_url()."'>Residence Online</a></b>.</p>
				<p>Para visualizar a nova comunicação, acesse o <b><a style='text-decoration: none;' href='".base_url()."'>Residence Online</a></b>.</p>
				";
				$this->enviarEmail("Uma comunicação com você foi aberta.", $destinatario->email, $mensagem);
			}

			if($destinatario->token_app != null && $destinatario->token_app != ""){
				if($destinatario->id != $this->session->userdata("id"))
					$this->enviarNotificacao($destinatario->id, "Comunique", "Uma nova conversa com você foi aberta.");
			}

			if($destinatario->token_app_admin != null && $destinatario->token_app_admin != ""){
				if($destinatario->id != $this->session->userdata("id"))
					$this->enviarNotificacaoAdmin($destinatario->id, "Comunique", "Uma nova conversa com você foi aberta.");
			}
		}

		echo '<script language="JavaScript">window.location="/comunique/ver/'.$id.'";</script> ';
	}

	function marcarResolvido(){
		$post = $this->input->post();
		$this->db->start_cache();
		$this->db->where("solicitacoes.cnpj", $this->session->userdata("cnpj"));
		$this->db->where("solicitacoes.id", $post['id']);
		$this->db->join("usuarios", "usuarios.id = solicitacoes.destinatario");
		$this->db->select("solicitacoes.*, usuarios.id as id_usuario, usuarios.email, usuarios.token_app, usuarios.token_app_admin");
		$solicitacao = $this->db->get("solicitacoes");
		$this->db->stop_cache();
		$this->db->flush_cache();
		if($solicitacao->num_rows() == 1){
			$solicitacao = $solicitacao->first_row();
			$this->db->where("solicitacoes.cnpj", $this->session->userdata("cnpj"));
			$this->db->where("solicitacoes.id", $post['id']);
			$this->db->update("solicitacoes", array("resolvida"=>true));
			$this->session->set_flashdata("mensagem_comunique", "<div class='alert alert-info'><h3 class='text-center'>Comunicação resolvida!</h3></div>");

			if($solicitacao->email != null && trim($solicitacao->email) != ""){
				$mensagem = "<p>Nome da comunicação: <b>$solicitacao->titulo</b>.</p>";
				$this->enviarEmail("A comunicação foi resolvida!", $solicitacao->email, $mensagem);
			}

			if($solicitacao->token_app != null && $solicitacao->token_app != ""){
				if($solicitacao->id_usuario != $this->session->userdata("id"))
					$this->enviarNotificacao($solicitacao->id_usuario, "Comunique", "Uma conversa com você foi resolvida.");
			}

			if($solicitacao->token_app_admin != null && $solicitacao->token_app_admin != ""){
				if($solicitacao->id_usuario != $this->session->userdata("id"))
					$this->enviarNotificacaoAdmin($solicitacao->id_usuario, "Comunique", "Uma conversa com você foi resolvida.");
			}
		}
	}

	// FIM CRUD DE SOLICITACOES ---------------------------------------------------------------------------
	// INÍCIO CRUD DE ÁREAS ------------------------------------------------------------------------
	function getAreas(){
		$this->db->where("areas.cnpj", $this->session->userdata("cnpj"));
		$this->db->join("usuarios", "usuarios.id = areas.usuario_responsavel", 'left');
		$this->db->join("localizacoes", "localizacoes.id = areas.localizacao", 'left');
		$this->db->select("areas.*, usuarios.nome as nome_responsavel, localizacoes.nome as nome_localizacao");
		return $this->db->get("areas");
	}

	function getArea($id = NULL){
		if($id != NULL){
			$this->db->where("areas.id", $id);
			$this->db->where("areas.cnpj", $this->session->userdata("cnpj"));
			$this->db->join("usuarios", "usuarios.id = areas.usuario_responsavel", 'left');
			$this->db->join("localizacoes", "localizacoes.id = areas.localizacao", 'left');
			$this->db->select("areas.*, usuarios.nome as nome_responsavel, localizacoes.nome as nome_localizacao");
			return $this->db->get("areas");
		}
	}

	function getHorariosArea($id = NULL){
		if($id != NULL){
			$this->db->where("horarios_areas.cnpj", $this->session->userdata("cnpj"));
			$this->db->where("horarios_areas.area", $id);
			return $this->db->get("horarios_areas");
		}
	}

	function reservar(){
		$post = $this->input->post();

		$post['horario_inicio'] = explode(" ", $post['horario_inicio']);
		$post['horario_inicio'][0] = explode("/", $post['horario_inicio'][0]);
		$post['horario_inicio'] = $post['horario_inicio'][0][2]."-".$post['horario_inicio'][0][1]."-".$post['horario_inicio'][0][0]." ".$post['horario_inicio'][1];

		$post['horario_final'] = explode(" ", $post['horario_final']);
		$post['horario_final'][0] = explode("/", $post['horario_final'][0]);
		$post['horario_final'] = $post['horario_final'][0][2]."-".$post['horario_final'][0][1]."-".$post['horario_final'][0][0]." ".$post['horario_final'][1];



		$dia_inicio = date("l", strtotime($post['horario_inicio']));
		$dia_final = date("l", strtotime($post['horario_final']));
		switch ($dia_inicio) {
			case 'Monday':
			$dia_inicio = "SEGUNDA";
			break;
			case 'Tuesday':
			$dia_inicio = "TERCA";
			break;
			case 'Wednesday':
			$dia_inicio = "QUARTA";
			break;
			case 'Thursday':
			$dia_inicio = "QUINTA";
			break;
			case 'Friday':
			$dia_inicio = "SEXTA";
			break;
			case 'Saturday':
			$dia_inicio = "SABADO";
			break;
			case 'Sunday':
			$dia_inicio = "DOMINGO";
			break;
			default:
			break;
		}
		switch ($dia_final) {
			case 'Monday':
			$dia_final = "SEGUNDA";
			break;
			case 'Tuesday':
			$dia_final = "TERCA";
			break;
			case 'Wednesday':
			$dia_final = "QUARTA";
			break;
			case 'Thursday':
			$dia_final = "QUINTA";
			break;
			case 'Friday':
			$dia_final = "SEXTA";
			break;
			case 'Saturday':
			$dia_final = "SABADO";
			break;
			case 'Sunday':
			$dia_final = "DOMINGO";
			break;
			default:
			break;
		}

		$this->db->start_cache();
		$reserva = $this->db->query("SELECT * FROM `reservas` WHERE (hora_inicio BETWEEN '$post[horario_inicio]' AND '$post[horario_final]') OR ((hora_fim BETWEEN '$post[horario_inicio]' AND '$post[horario_final]')) AND `reservas`.`area` = '20' AND `reservas`.`autorizado` = 1")->num_rows();
		// echo $this->db->last_query();
		$this->db->stop_cache();
		$this->db->flush_cache();
		if($reserva == 0){

			if(strtotime($post['horario_final']) > strtotime($post['horario_inicio'])){
				if(strtotime($post['horario_inicio']) <= strtotime(date("Y-m-d H:i:s"))){
					echo "<p>A data inicial deve ser posterior ao dia de hoje.</p>";
				}else{
					$horario_inicio = date("H:i:s", strtotime($post['horario_inicio']));
					$horario_final = date("H:i:s", strtotime($post['horario_final']));
					$this->db->start_cache();
					$this->db->where("horarios_areas.cnpj", $this->session->userdata("cnpj"));
					$this->db->where("horarios_areas.area", $post['area']);

					$this->db->where("(horarios_areas.horario_inicio <= '$horario_inicio') AND (horarios_areas.horario_final >= '$horario_final') AND (horarios_areas.dia_repetivel = '$dia_inicio' OR horarios_areas.dia_repetivel = '".date("Y-m-d", strtotime($post['horario_inicio']))."') AND (horarios_areas.dia_repetivel = '$dia_final' OR horarios_areas.dia_repetivel = '".date("Y-m-d", strtotime($post['horario_final']))."')");

					$horarios = $this->db->get("horarios_areas");
					$total = $horarios->num_rows();
					if($total > 0){
						$dados = array(
							'usuario'=>$this->session->userdata("id"),
							'operador_alteracao'=>$this->session->userdata("id"),
							'area'=>$post['area'],
							'cnpj'=>$this->session->userdata("cnpj"),
							'hora_inicio'=>date("Y-m-d H:i:s", strtotime($post['horario_inicio'])),
							'hora_fim'=>date("Y-m-d H:i:s", strtotime($post['horario_final'])),
							'data_alteracao'=>date("Y-m-d H:i:s")
							);
						$this->db->insert("reservas", $dados);
						$inserido = $this->db->insert_id();
						$this->session->set_flashdata("mensagem_reserva", "<h2 class='text-info text-center'>Reserva efetuada!<br /><small class='text-info'>Aguarde aprovação do responsável.</small></h2>");

						$this->db->start_cache();
						$this->db->where("reservas.id", $inserido);
						$this->db->join("areas", "areas.id = reservas.area");
						$this->db->join("usuarios as responsavel", "areas.usuario_responsavel = responsavel.id");
						$this->db->join("usuarios as requerente", "reservas.usuario = requerente.id");
						$this->db->select("reservas.hora_inicio, reservas.hora_fim, areas.nome as nome_area, responsavel.id as id_responsavel, responsavel.email as email_responsavel, requerente.nome as nome_requerente, requerente.id as id_requerente, responsavel.token_app, responsavel.token_app_admin");
						$reserva = $this->db->get("reservas")->first_row();
						$this->db->stop_cache();
						$this->db->flush_cache();
						if($reserva->email_responsavel != null && trim($reserva->email_responsavel) != ""){
							$mensagem = "
							<b>$reserva->nome_requerente</b> solicitou uma reserva em <b>$reserva->nome_area</b> de 
							".date("d/m/Y H:i:s", strtotime($reserva->hora_inicio))." até ".date("d/m/Y H:i:s", strtotime($reserva->hora_fim)).". Para autorizar esta reserva, acesse a sua <a href='https://admin.acessonaweb.com.br'>área administrativa</a>.";
							$this->enviarEmail("Nova reserva em $reserva->nome_area", $reserva->email_responsavel, $mensagem);
						}

						if($reserva->token_app_admin != null && $reserva->token_app_admin != ""){
							if($reserva->id_responsavel != $this->session->userdata("id"))
								$this->enviarNotificacaoAdmin($reserva->id_responsavel, $reserva->nome_area, "$reserva->nome_requerente solicitou uma reserva.");
						}

					}else{
						echo "<p>Horário indisponível.</p>";
					}
					$this->db->stop_cache();
					$this->db->flush_cache();
				}
			}else{
				echo "<p>A data inicial deve ser menor do que a data final.</p>";
			}
		}else{
			echo "<p>Já existe uma reserva confirmada neste horário.</p>";
		}
	}

	function ajax_cancelar_reserva(){
		$post = $this->input->post();
		

		$this->db->start_cache();
		$this->db->where("reservas.id", $post['id']);
		$this->db->join("areas", "areas.id = reservas.area");
		$this->db->join("usuarios as responsavel", "areas.usuario_responsavel = responsavel.id");
		$this->db->join("usuarios as requerente", "reservas.usuario = requerente.id");
		$this->db->select("reservas.hora_inicio, reservas.hora_fim, areas.nome as nome_area, responsavel.email as email_responsavel, requerente.nome as nome_requerente, responsavel.id as id_responsavel, responsavel.token_app_admin");
		$reserva = $this->db->get("reservas")->first_row();
		$this->db->stop_cache();
		$this->db->flush_cache();
		if($reserva->email_responsavel != null && trim($reserva->email_responsavel) != ""){
			$mensagem = "
			<b>$reserva->nome_requerente</b> solicitou uma reserva em <b>$reserva->nome_area</b> de 
			".date("d/m/Y H:i:s", strtotime($reserva->hora_inicio))." até ".date("d/m/Y H:i:s", strtotime($reserva->hora_fim))." A reserva foi <b>cancelada pelo usuário e não requer mais sua atenção.</b>.";
			$this->enviarEmail("A reserva em $reserva->nome_area foi cancelada.", $reserva->email_responsavel, $mensagem);
		}

		if($reserva->token_app_admin != null && $reserva->token_app_admin != ""){
			if($reserva->id_responsavel != $this->session->userdata("id"))
				$this->enviarNotificacaoAdmin($reserva->id_responsavel, $reserva->nome_area, "$reserva->nome_requerente cancelou a reserva.");
		}

		$this->db->where("cnpj", $this->session->userdata("cnpj"));
		$this->db->where("usuario", $this->session->userdata("id"));
		$this->db->where("id", $post['id']);
		$this->db->delete("reservas");
		$this->session->set_flashdata("mensagem_reserva", "<h2 class='text-danger text-center'>Reserva cancelada!</h2>");
	}

	function getConvidados(){
		$post = $this->input->post();
		$this->db->where("cnpj", $this->session->userdata("cnpj"));
		$this->db->where("reserva", $post['id']);
		$this->db->order_by("nome", "ASC");
		return $this->db->get("convidados_reservas");
	}

	function adicionar_convidado(){
		$post = $this->input->post();
		$post['cnpj'] = $this->session->userdata("cnpj");
		$post['operador_alteracao'] = $this->session->userdata("id");
		$post['data_alteracao'] = date("Y-m-d H:i:s");
		$this->db->insert("convidados_reservas", $post);
	}

	function ajax_remover_convidado(){
		$post = $this->input->post();
		$this->db->where("id", $post['id']);
		$this->db->delete("convidados_reservas");
	}
	// FIM CRUD DE ÁREAS ---------------------------------------------------------------------
	// INÍCIO CRUD RESERVAS ------------------------------------------------------------------
	function getReservas($inicio = null, $offset = null){
		$this->db->select("reservas.*, areas.nome as nome_area");
		$this->db->join("areas", "areas.id = reservas.area");
		$this->db->limit($offset, $inicio);
		$this->db->where("reservas.usuario", $this->session->userdata("id"));
		$this->db->where("reservas.hora_fim > NOW()");
		return $this->db->get("reservas");
	}

	function getReservasPassadas($inicio = null, $offset = null){
		$this->db->select("reservas.*, areas.nome as nome_area");
		$this->db->join("areas", "areas.id = reservas.area");
		$this->db->limit($offset, $inicio);
		$this->db->where("reservas.usuario", $this->session->userdata("id"));
		$this->db->where("reservas.hora_fim < NOW()");
		return $this->db->get("reservas");
	}

	function getReserva($id){
		$this->db->select("reservas.*");
		$this->db->where("reservas.usuario", $this->session->userdata("id"));
		$this->db->where("reservas.id", $id);
		return $this->db->get("reservas");
	}
	// FIM CRUD DE RESERVAS ------------------------------------------------------------------
	// INÍCIO CRUD VEÍCULOS ------------------------------------------------------------------

	function getVeiculos(){
		$this->db->where("veiculos.cnpj", $this->session->userdata("cnpj"));
		$this->db->where("usuario_responsavel", $this->session->userdata("id"));
		$this->db->join("residencias", "residencias.id = veiculos.residencia", "left");
		$this->db->join("cadastros_gerais", "cadastros_gerais.id = veiculos.tipo", "left");
		$this->db->select("veiculos.*, cadastros_gerais.nome as tipo_veiculo");
		$this->db->order_by("id", 'DESC');
		return $this->db->get("veiculos");
	}

	function getVeiculo($id){
		$this->db->where("veiculos.id", $id);
		$this->db->where("veiculos.cnpj", $this->session->userdata("cnpj"));
		$this->db->where("usuario_responsavel", $this->session->userdata("id"));
		$this->db->join("residencias", "residencias.id = veiculos.residencia", "left");
		$this->db->join("usuarios", "residencias.id = usuarios.residencia", "left");
		$this->db->join("cadastros_gerais", "cadastros_gerais.id = veiculos.tipo", "left");
		$this->db->select("veiculos.*, cadastros_gerais.nome as tipo_veiculo");
		return $this->db->get("veiculos");
	}

	function atualizar_foto_veiculo(){
		$post = $this->input->post();
		$this->db->start_cache();
		$this->db->where("veiculos.id", $post['id']);
		$this->db->where("veiculos.cnpj", $this->session->userdata("cnpj"));
		$this->db->select("veiculos.foto, veiculos.id");
		$veiculo = $this->db->get("veiculos");
		$this->db->stop_cache();
		$this->db->flush_cache();
		$total = $veiculo->num_rows();
		if($total == 1){
			$veiculo = $veiculo->first_row();
			$this->db->where("veiculos.id", $post['id']);
			$this->db->where("veiculos.cnpj", $this->session->userdata("cnpj"));
			$this->db->update("veiculos", array('foto'=>$post['foto']));
		}
	}

	function editable_veiculo($name, $value, $pk){
		$this->db->where("id", $pk);
		$this->db->where("cnpj", $this->session->userdata("cnpj"));
		$array = array(
			"$name"=>"$value"
			);
		$this->db->update("veiculos", $array);
	}

	function deletar_veiculo(){
		$post = $this->input->post();
		if(is_numeric($post['id'])){
			$this->db->start_cache();
			$this->db->where("id", $post['id']);
			$this->db->where("cnpj", $this->session->userdata("cnpj"));
			$this->db->delete("veiculos");
			$this->db->stop_cache();
			$this->db->flush_cache();
			$this->session->set_flashdata("mensagem_cadastro_veiculo", "Removido com sucesso");
		}
	}

	function cadastrar_veiculo(){
		$post = $this->input->post();
		if($this->logado()){
			$this->db->start_cache();
			$this->db->where("usuarios.id", $this->session->userdata("id"));
			$this->db->where("usuarios.cnpj", $this->session->userdata("cnpj"));
			$this->db->select("usuarios.residencia, usuarios.id");
			$usuario = $this->db->get("usuarios");
			$this->db->stop_cache();
			$this->db->flush_cache();
			if($usuario->num_rows() > 0){
				$usuario = $usuario->first_row();
				$post = $this->input->post();
				$post['operador_alteracao'] = $this->session->userdata("id");
				$post['usuario_responsavel'] = $this->session->userdata("id");
				$post['residencia'] = $usuario->residencia;
				$post['situacao'] = true;
				$post['cnpj'] = $this->session->userdata("cnpj");
				$post['data_alteracao'] = date("Y-m-d H:i:s");
				
				if($this->db->insert("veiculos", $post)){
					$this->session->set_flashdata("mensagem_cadastro_veiculo", "Veículo cadastrado com sucesso!");
					redirect("/veiculos");
				}
			}
		}
	}

	// FIM CRUD VEÍCULOS    -----------------------------------------------------------------
	// INÍCIO CRUD ENQUETES    -----------------------------------------------------------------
	function criar_enquete(){
		$post = $this->input->post();
		$post['data_validade'] = explode(" ", $post['data_validade']);
		$post['data_validade'][0] = explode("/", $post['data_validade'][0]);
		$post['data_validade'] = $post['data_validade'][0][2]."-".$post['data_validade'][0][1]."-".$post['data_validade'][0][0]." ".$post['data_validade'][1];
		$this->db->start_cache();
		$dados = array(
			"titulo"=>$post['titulo'],
			"descricao"=>$post['descricao'],
			"data_validade"=>$post['data_validade'],
			"usuario"=>$this->session->userdata("id"),
			"cnpj"=>$this->session->userdata("cnpj"),
			"operador_alteracao"=>$this->session->userdata("id"),
			'data_abertura' => date("Y-m-d H:i:s"),
			'data_alteracao'=>date("Y-m-d H:i:s")
			);
		$this->db->insert("enquetes", $dados);
		$inserido = $this->db->insert_id();
		$this->db->stop_cache();
		$this->db->flush_cache();

		foreach($post['opcoes'] as $opcao){
			$dados = array(
				"titulo"=>$opcao,
				"enquete"=>$inserido,
				"cnpj"=>$this->session->userdata("cnpj"),
				"operador_alteracao"=>$this->session->userdata("id"),
				'data_alteracao'=>date("Y-m-d H:i:s")
				);
			$this->db->insert("opcoes_enquetes", $dados);
		}
		$this->session->set_flashdata("mensagem_enquete", "Enquete criada com sucesso.");
		$this->db->start_cache();
		$this->db->where("enquetes.id", $inserido);
		$this->db->join("usuarios", "usuarios.id = enquetes.usuario");
		$this->db->select("usuarios.email as email_usuario, enquetes.*");
		$enquete = $this->db->get("enquetes")->first_row();
		$this->db->stop_cache();
		$this->db->flush_cache();


		$this->db->start_cache();
		$this->db->where("usuarios.cnpj", $this->session->userdata("cnpj"));
		$this->db->where("usuarios.id != ".$this->session->userdata('id'));
		$this->db->select("usuarios.email as email_usuario, usuarios.id, usuarios.token_app_admin, usuarios.token_app");
		$usuarios = $this->db->get("usuarios")->result();
		$this->db->stop_cache();
		$this->db->flush_cache();

		foreach($usuarios as $usuario){
			if($usuario->email_usuario != null && trim($usuario->email_usuario) != ""){
				$mensagem = "
				Uma nova enquete está aberta no seu condomínio. Sua opinião é muito importante.
				<h3>$enquete->titulo</h3>
				Para ver a enquete, acesse o aplicativo ou site do <a href='".base_url()."'><b>Residence Online</b></a> e dê sua opinião.<br />
				";
				$this->enviarEmail("Nova enquete aberta no condomínio.", $usuario->email_usuario, $mensagem);
			}

			if($usuario->token_app != null && $usuario->token_app != ""){
				if($usuario->id != $this->session->userdata("id"))
					$this->enviarNotificacao($usuario->id, "Nova enquete!", "$enquete->titulo");
			}

			if($usuario->token_app_admin != null && $usuario->token_app_admin != ""){
				if($usuario->id != $this->session->userdata("id"))
					$this->enviarNotificacaoAdmin($usuario->id, "Nova enquete!", "$enquete->titulo");
			}
		}

		redirect("/enquetes");
	}

	function getEnquetes($inicio = NULL, $offset = NULL){
		$this->db->limit($offset, $inicio);
		$this->db->where("enquetes.cnpj", $this->session->userdata("cnpj"));
		$this->db->where("(enquetes.ativado = 1 OR enquetes.usuario = ".$this->session->userdata("id").")");
		$this->db->join("usuarios", "usuarios.id = enquetes.usuario", 'left');

		$this->db->join("opcoes_enquetes", "enquetes.id = opcoes_enquetes.enquete", 'left');
		$this->db->join("votos_enquetes", "opcoes_enquetes.id = votos_enquetes.voto", 'left');

		$this->db->select("enquetes.*, usuarios.nome as nome_usuario, COUNT(votos_enquetes.id) as total_votos");
		$this->db->group_by("enquetes.id");
		$this->db->order_by("enquetes.data_validade DESC, enquetes.ativado DESC, enquetes.id DESC");
		return $this->db->get("enquetes");
	}

	function getEnquete($id){
		$this->db->where("enquetes.id", $id);
		$this->db->where("enquetes.cnpj", $this->session->userdata("cnpj"));
		$this->db->join("usuarios", "usuarios.id = enquetes.usuario", 'left');
		$this->db->select("enquetes.*, usuarios.nome as nome_usuario");
		$this->db->order_by("enquetes.id", "DESC");
		return $this->db->get("enquetes");
	}

	function getVotoEnquete($enquete){ //verificando se o usuário já votou na enquete
		$this->db->where("votos_enquetes.usuario", $this->session->userdata("id"));
		$this->db->where("enquetes.id", $enquete);
		$this->db->where("usuarios.cnpj", $this->session->userdata("cnpj"));
		$this->db->join("opcoes_enquetes", "opcoes_enquetes.id = votos_enquetes.voto", 'left');
		$this->db->join("enquetes", "enquetes.id = opcoes_enquetes.enquete", 'left');
		$this->db->join("usuarios", "usuarios.id = enquetes.usuario", 'left');
		$this->db->select("votos_enquetes.voto, opcoes_enquetes.titulo, votos_enquetes.data_alteracao");
		return $this->db->get("votos_enquetes");
	}

	function votosEnquete(){
		$this->db->where("votos_enquetes.cnpj", $this->session->userdata("cnpj"));
		$this->db->group_by("opcoes_enquetes.titulo");
		$this->db->join("opcoes_enquetes", "opcoes_enquetes.id = votos_enquetes.voto");
		$this->db->join("enquetes", "enquetes.id = opcoes_enquetes.enquete");
		$this->db->select("COUNT(votos_enquetes.id) as total_votos, opcoes_enquetes.titulo as nome_opcao, enquetes.titulo as nome_enquete");
		return $this->db->get("votos_enquetes");
	}

	function getOpcoesEnquete($enquete){
		$this->db->where("opcoes_enquetes.enquete", $enquete);
		$this->db->where("opcoes_enquetes.cnpj", $this->session->userdata("cnpj"));
		$this->db->join("votos_enquetes", "votos_enquetes.voto = opcoes_enquetes.id", 'LEFT');
		$this->db->select("opcoes_enquetes.*, COUNT(votos_enquetes.id) as total_votos");
		$this->db->order_by("opcoes_enquetes.id", "ASC");
		$this->db->group_by("opcoes_enquetes.titulo");
		return $this->db->get("opcoes_enquetes");
	}

	function votarEnquete(){
		$post = $this->input->post();
		$post['usuario'] = $this->session->userdata("id");
		$post['cnpj'] = $this->session->userdata("cnpj");
		$post['operador_alteracao'] = $this->session->userdata("id");
		$post['data_alteracao'] = date("Y-m-d H:i:s");
		$this->db->insert("votos_enquetes", $post);
		$this->session->set_flashdata("mensagem_enquete", "Você votou com sucesso!");
		redirect("/enquetes");
	}

	function alterar_validade_enquete(){
		$post = $this->input->post();
		$post['data_validade'] = explode(" ", $post['data_validade']);
		$post['data_validade'][0] = explode("/", $post['data_validade'][0]);
		$post['data_validade'] = $post['data_validade'][0][2]."-".$post['data_validade'][0][1]."-".$post['data_validade'][0][0]." ".$post['data_validade'][1];
		$dados = array(
			'data_validade'=>$post['data_validade']
			);
		$this->db->where("enquetes.id", $post['id']);
		$this->db->update("enquetes", $dados);
		$this->session->set_flashdata("mensagem_enquete", "Alterado com sucesso.");
	}

	function desativar_enquete(){
		$post = $this->input->post();
		
		// $dados = array(
		// 	'ativado'=>false
		// );
		// $this->db->where("enquetes.id", $post['id']);
		// $this->db->update("enquetes", $dados);
		$this->db->query("UPDATE enquetes SET ativado = !ativado WHERE id = $post[id]");
		$this->session->set_flashdata("mensagem_enquete", "Alterado com sucesso.");
	}
	// FIM CRUD ENQUETES    -----------------------------------------------------------------
	// INÍCIO CRUD FÓRUM    -----------------------------------------------------------------

	function cadastrar_discussao(){
		$post = $this->input->post();
		$post['cnpj'] = $this->session->userdata("cnpj");
		$post['operador_alteracao'] = $this->session->userdata("id");
		$post['usuario'] = $this->session->userdata("id");
		$post['data_abertura'] = date("Y-m-d H:i:s");
		$post['data_alteracao'] = date("Y-m-d H:i:s");
		$this->db->insert("discussoes_forum", $post);
		$this->session->set_flashdata("mensagem_forum", "Discussão criada com sucesso!");
		redirect("/forum");
	}

	function getDiscussoes($inicio = NULL, $offset = NULL){
		$this->db->limit($offset, $inicio);
		$this->db->where("discussoes.cnpj", $this->session->userdata("cnpj"));
		// $this->db->where("(discussoes.ativado = 1 OR discussoes.usuario = ".$this->session->userdata("id").")");
		$this->db->join("usuarios", "usuarios.id = discussoes.usuario", "left");
		$this->db->select("discussoes.*, usuarios.nome as nome_usuario");
		$this->db->order_by("id", "DESC");
		return $this->db->get("discussoes_forum as discussoes");
	}

	function getDiscussao($id){
		$this->db->where("discussoes.id", $id);
		$this->db->where("discussoes.cnpj", $this->session->userdata("cnpj"));
		// $this->db->where("(discussoes.ativado = 1 OR discussoes.usuario = ".$this->session->userdata("id").")");
		$this->db->join("usuarios", "usuarios.id = discussoes.usuario", "left");
		$this->db->select("discussoes.*, usuarios.nome as nome_usuario, usuarios.foto as foto_usuario");
		return $this->db->get("discussoes_forum as discussoes");
	}

	function responder_discussao($id = NULL){
		if($id != NULL){
			$post = $this->input->post();
			$dados = array(
				'cnpj'=>$this->session->userdata("cnpj"),
				'operador_alteracao'=>$this->session->userdata("id"),
				'usuario'=>$this->session->userdata("id"),
				'discussao'=>$id,
				'resposta'=>$post['resposta'],
				'data_alteracao'=>date("Y-m-d H:i:s")
			);
			$this->db->insert("respostas_discussoes", $dados);
			$this->session->set_flashdata("mensagem_discussao", "Respondido com sucesso!");

			$inserido = $this->db->insert_id();
			$this->db->start_cache();
			$this->db->where("respostas_discussoes.id", $inserido);
			$this->db->where("discussoes_forum.usuario != ".$this->session->userdata("id"));
			$this->db->join("discussoes_forum", "respostas_discussoes.discussao = discussoes_forum.id");
			$this->db->join("usuarios as dono", "dono.id = discussoes_forum.usuario");
			$this->db->join("usuarios as respondente", "respondente.id = respostas_discussoes.usuario");
			$this->db->select("discussoes_forum.titulo as nome_discussao, dono.email as email_dono, respondente.email as email_respondente, dono.token_app, dono.token_app_admin, dono.id as id_dono");
			$resposta = $this->db->get("respostas_discussoes")->first_row();
			$this->db->flush_cache();
			$this->db->stop_cache();

			if($resposta != null){
				if($resposta->email_dono != null){
					$mensagem = "
					<p>Você recebeu uma nova mensagem na sua discussão <b>$resposta->nome_discussao</b>.</p>
					<p>Para visualizar a nova mensagem, entre no <b>Residence Online</b>.</p>
					";
					$this->enviarEmail("Você recebeu uma resposta no fórum.", $resposta->email_dono, $mensagem);
				}

				if($resposta->token_app != null && $resposta->token_app != ""){
					if($resposta->id_dono != $this->session->userdata("id"))
						$this->enviarNotificacao($resposta->id_dono, "Resposta no Fórum", "$resposta->nome_discussao");
				}

				if($resposta->token_app_admin != null && $resposta->token_app_admin != ""){
					if($resposta->id_dono != $this->session->userdata("id"))
						$this->enviarNotificacaoAdmin($resposta->id_dono, "Resposta no Fórum", "$resposta->nome_discussao");
				}
			}
		}
	}

	function getRespostasDiscussao($inicio = NULL, $offset = NULL, $id = NULL){
		$this->db->limit($offset, $inicio);
		$this->db->where("respostas_discussoes.discussao", $id);
		$this->db->join("usuarios", "usuarios.id = respostas_discussoes.usuario");
		$this->db->order_by("respostas_discussoes.id", "ASC");
		$this->db->select("respostas_discussoes.*, usuarios.nome as nome_usuario, usuarios.foto as foto_usuario");
		$this->db->order_by("id", "ASC");
		return $this->db->get("respostas_discussoes");
	}

	function desativar_discussao(){
		$post = $this->input->post();
		$this->db->query("UPDATE discussoes_forum SET ativado = !ativado WHERE id = $post[id]");
		$this->session->set_flashdata("mensagem_forum", "Alterado com sucesso.");
	}
	// FIM CRUD FÓRUM    -----------------------------------------------------------------
	// INÍCIO CRUD AVISOS    -------------------------------------------------------------
	function getAvisos($inicio = null, $offset = null){
		// if($inicio != null && $offset != null)
		$this->db->limit($offset, $inicio);
		$this->db->where("avisos.cnpj", $this->session->userdata("cnpj"));
		$this->db->where("avisos.administradores", 0);
		$this->db->where("avisos.data_validade >= '".date("Y-m-d H:i:s")."'");
		$this->db->select("avisos.id, avisos.titulo, avisos.descricao, avisos.data_validade, avisos.operador_alteracao, avisos.data_alteracao");
		$this->db->order_by("avisos.id", 'DESC');
		return $this->db->get("avisos");
	}

	function getAviso(){
		$post = $this->input->post();
		$this->db->where("avisos.id", $post['id']);
		$this->db->where("avisos.cnpj", $this->session->userdata("cnpj"));
		$this->db->select("avisos.id, avisos.titulo, avisos.descricao, avisos.data_validade");
		return $this->db->get("avisos");
	}

	function cadastrar_aviso(){
		$post = $this->input->post();
		$post['data_validade'] = explode("/", $post['data_validade']);
		$post['data_validade'] = $post['data_validade'][2]."-".$post['data_validade'][1]."-".$post['data_validade'][0];
		if((strtotime($post['data_validade']) <= strtotime(date("Y-m-d")))){
			echo "<p>Deve informar uma data posterior ao dia de hoje.</p>";
			exit();
		}
		$dados = array(
			'titulo'=>$post['titulo'],
			'descricao'=>$post['descricao'],
			'data_validade'=>$post['data_validade']." 23:59:59",
			'cnpj'=>$this->session->userdata("cnpj"),
			'operador_alteracao'=>$this->session->userdata("id"),
			'data_alteracao'=>date("Y-m-d H:i:s")
		);
		$this->db->insert("avisos", $dados);
		$this->session->set_flashdata("mensagem_cadastro_aviso", "Aviso cadastrado com sucesso.");
		$this->db->start_cache();
		$this->db->where("usuarios.cnpj", $this->session->userdata("cnpj"));
		$this->db->where("usuarios.email != ''");
		$this->db->where("usuarios.email IS NOT null");
		$this->db->where("usuarios.id != ".$this->session->userdata("id"));
		$usuarios = $this->db->get("usuarios")->result();
		$this->db->flush_cache();
		$this->db->stop_cache();

		foreach($usuarios as $usuario){
			if($usuario->email != null && $usuario->email != ""){
				$mensagem = "
				<p>Olá $usuario->nome,</p>
				<p>Um novo aviso para os moradores está disponível.</p>
				<p>Verifique o <b><a href='".base_url()."'>Residence Online</a></b> para visualizar o aviso.</p>
				";
				$this->enviarEmail("Novo aviso no condomínio.", $usuario->email, $mensagem);
			}

			if($usuario->token_app != null && $usuario->token_app != ""){
				if($usuario->id != $this->session->userdata("id"))
					$this->enviarNotificacao($usuario->id, "Novo aviso", "$post[titulo]");
			}

			if($usuario->token_app_admin != null && $usuario->token_app_admin != ""){
				if($usuario->id != $this->session->userdata("id"))
					$this->enviarNotificacaoAdmin($usuario->id, "Novo aviso", "$post[titulo]");
			}
		}
	}

	function deletarAviso(){
		$post = $this->input->post();
		$this->db->where("avisos.id", $post['id']);
		$this->db->where("avisos.cnpj", $this->session->userdata("cnpj"));
		$this->db->where("avisos.operador_alteracao", $this->session->userdata("id"));
		$this->db->delete("avisos");
		$this->session->set_flashdata("mensagem_cadastro_aviso", "Aviso removido com sucesso.");
	}
	// FIM CRUD AVISOS    ----------------------------------------------------------------
	// INÍCIO DOCUMENTOS ------------------------------------------------------------------------------
	function getDocumentos($inicio = null, $offset = null, $keyword = null){
		$this->db->limit($offset, $inicio);
		if($keyword != null && $keyword != ""){
			$this->db->like("titulo", $keyword);
		}
		$this->db->where("documentos.cnpj", $this->session->userdata("cnpj"));
		return $this->db->get("documentos");
	}

	function getDocumento($id = null){
		$this->db->where("documentos.cnpj", $this->session->userdata("cnpj"));
		$this->db->where("documentos.id", $id);
		return $this->db->get("documentos");
	}
	// FIM DOCUMENTOS ---------------------------------------------------------------------------------
}